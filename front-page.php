<?php
/**
 * The front page template file
 *
 * @package Spawn Ideas
 * @since 0.1.0
 */

$category_title     = get_option( 'spawn_category_title_text' );
$category_text      = get_option( 'spawn_featured_category_text' );
$category_link_text = get_option( 'spawn_category_link_text' );
$category_link      = get_option( 'spawn_category_link' );
$best_places_title  = get_option( 'spawn_best_places_title' );
$best_places_text   = get_option( 'spawn_best_places_text' );

get_header(); ?>

<div id="primary">

  <div class="row content-bg">
    <div class="narrow block-left co-logo">
      <img src="<?php echo get_field('left_images')['url']; ?>" alt="<?php echo get_field('left_images')['alt']; ?>" />
    </div>
    
    <div class="half block-left home-content">
    <?php /* Start the Loop */ ?>
		<?php while ( have_posts() ) : the_post(); ?>
      
      <?php the_content(); ?>
    
    <?php endwhile; ?>
		
      <div class="home-cta">
      
      <a href="<?php the_field('button_text_link_1'); ?>"><?php the_field('button_text_1'); ?></a> 
      
      </div>
				
		</div>
				
    <div class="narrow block-left co-logo">
      <img src="<?php echo get_field('right_images')['url']; ?>" alt="<?php echo get_field('right_images')['alt']; ?>" />  
    </div>
    
  </div>

		<div class="row">
    <?php get_template_part( 'parts/home-work' ); ?>
		</div>

	<div class="row">
		<?php echo do_shortcode('[rev_slider home_slider_2]'); ?>
  </div>	

  <div class="row">
    <?php get_template_part( 'parts/home-posts' ); ?>
  </div>

		<div class="row home-gallery">
		<div class="half block-left large">
  		<img src="<?php echo get_field('staff_image_large')['url']; ?>" alt="<?php echo get_field('staff_image_large')['alt']; ?>" />
    </div>
			<div class="half block-left">
  			
  			<div class="half block-left">
  			<img src="<?php echo get_field('staff_image_small_1')['url']; ?>" alt="<?php echo get_field('staff_image_small_1')['alt']; ?>" /><img src="<?php echo get_field('staff_image_small_2')['url']; ?>" alt="<?php echo get_field('staff_image_small_2')['alt']; ?>" />
  		  </div>
  			
  		<div class="half block-left">
  			<img src="<?php echo get_field('staff_image_small_3')['url']; ?>" alt="<?php echo get_field('staff_image_small_3')['alt']; ?>" /><img src="<?php echo get_field('staff_image_small_4')['url']; ?>" alt="<?php echo get_field('staff_image_small_4')['alt']; ?>" />
  		  </div>
		</div>
		</div>
		
		

		

		<div class="row careers">
			<div class="narrow block-left career-header">
    			<h2>Best<br>Places To Work</h2></div>
			<div class="wide block-right">
			<h4 id="js-best-places-title"><?php esc_html_e( $best_places_title ); ?></h4>
			<div id="js-best-places-text"><p><?php esc_html_e( $best_places_text ); ?></p></div>
			<a href="/careers"><h5>&rarr; <?php esc_html_e( 'CAREERS', 'spawn' ); ?></a></h5></div>
		</div>

	</div><!-- #primary -->
		<?php get_template_part( 'parts/contact-row' ); ?>

<?php get_footer(); ?>
