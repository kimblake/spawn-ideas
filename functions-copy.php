<?php
/**
 * Spawn Ideas functions and definitions
 *
 * When using a child theme (see http://codex.wordpress.org/Theme_Development and
 * http://codex.wordpress.org/Child_Themes), you can override certain functions
 * (those wrapped in a function_exists() call) by defining them first in your child theme's
 * functions.php file. The child theme's functions.php file is included before the parent
 * theme's file, so the child theme functions would be used.
 *
 * @package Spawn Ideas
 * @since 0.1.0
 */

 // Useful global constants
define( 'SPAWN_VERSION', '1.0.3' );
define( 'CFCT_BUILD_TAXONOMY_LANDING', false );
define( 'SWIFTSTREAM_PATH', get_template_directory_uri() . "/includes/swiftstream/" );

// un-minify
define( 'SCRIPT_DEBUG', true );

 /**
  * Set up theme defaults and register supported WordPress features.
  *
  * @uses load_theme_textdomain() For translation/localization support.
  *
  * @since 0.1.0
  */
 function spawn_setup() {
	/**
	 * Makes Spawn Ideas available for translation.
	 *
	 * Translations can be added to the /lang directory.
	 * If you're building a theme based on Spawn Ideas, use a find and replace
	 * to change 'spawn' to the name of your theme in all template files.
	 */
	load_theme_textdomain( 'spawn', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	//
	add_theme_support( 'post-thumbnails' );

	// This theme uses wp_nav_menu() in two locations.
	register_nav_menus( array(
		'primary' => __( 'Primary Menu', 'spawn' ),
		'dropdown' => __( 'Dropdown Menu', 'spawn' )
	) );

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form', 'comment-form', 'comment-list', 'gallery', 'caption'
	) );

	/*
	 * Custom Image sizes
	 */
	add_image_size( 'square-small', 100, 100, true ); // tiny squares - posts footer
	add_image_size( 'square', 300, 300, true ); // larger squares - portfolio grid
	add_image_size( 'square-large', 600, 600, true ); // larger squares - services grid
	add_image_size( 'full-width', 1200, 300, true ); // full width
	add_image_size( 'half-width', 600, 320, true ); // half-width opposite content blocks
	add_image_size( 'headshot', 320, 160, true ); // headshots - staff page. 1/4 half-width
	add_image_size( 'blog-column', 720, 9999, false ); // Full width of a blog post.

	/*
	 * Set up taxonomies
	 */

	$keyword = new Taxonomy( array(
		'name'      => 'Keyword',
		'slug'      => 'keywords',
		'menu'      => true,
		'post_type' => array( 'spawn-projects', 'post' ),
	) );

	$services = new Taxonomy( array(
		'name'      => 'Service',
		'slug'      => 'services',
		'menu'      => true,
		'post_type' => array( 'spawn-projects', 'post' ),
		'fields'	=> array(
            array(
				        'id'          => 'service_tagline',
				        'label'       => 'Tag Line',
				        'type'        => 'text',
			      ),
            array(
				        'id'          => 'service_cta_text',
				        'label'       => 'CTA Text',
				        'type'        => 'text',
			      ),
		        array(
				        'id'          => 'service_cta_url',
				        'label'       => 'CTA url',
				        'type'        => 'text',
			      ),
	      )
	) );

	$partners = new Taxonomy( array(
		'name'      => 'Partner',
		'slug'      => 'partners',
		'menu'      => true,
		'post_type' => array( 'spawn-projects' ),
		'fields'    => array(
			array(
				'id'          => 'tax_image',
				'label'       => 'Affiliate logo',
				'description' => 'Maximum display size: 300 x 300 pixels',
				'type'        => 'image',
			),
			array(
				'id'          => 'tax_image_id',
				'type'        => 'hidden',
				'label'		  => '',
			),
			array(
				'id'          => 'partner_url',
				'label'       => 'URL',
				'type'        => 'text',
			),
		)
	) );

	$brands = new Taxonomy( array(
		'name'      => 'Brand',
		'slug'      => 'brands',
		'menu'      => true,
		'post_type' => array( 'spawn-projects' ),
	) );

	$awards = new Taxonomy( array(
		'name'      => 'Award',
		'slug'      => 'awards',
		'menu'      => true,
		'post_type' => array( 'spawn-projects' ),
	) );

 }
 add_action( 'after_setup_theme', 'spawn_setup' );

 /**
  * Enqueue scripts and styles for front-end.
  *
  * @since 0.1.0
  */
 function spawn_scripts_styles() {
	$postfix = ( defined( 'SCRIPT_DEBUG' ) && true === SCRIPT_DEBUG ) ? '' : '.min';

	wp_enqueue_script( 'spawn', get_template_directory_uri() . "/assets/js/spawn_ideas{$postfix}.js", array( 'jquery' ), SPAWN_VERSION, true );

//	wp_enqueue_style( 'spawn', get_template_directory_uri() . "/assets/css/spawn_ideas{$postfix}.css", array(), SPAWN_VERSION );
	wp_enqueue_style( 'spawn', get_template_directory_uri() . "/assets/css/spawn_ideas.css", array(), SPAWN_VERSION );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}

	if ( is_front_page() ) {
		wp_enqueue_script( 'responsiveslides', get_template_directory_uri() . '/assets/js/vendor/responsiveslides.min.js', array( 'jquery' ) );
	}

//	if ( wp_is_mobile() && is_singular( 'spawn-projects' ) ) {
		wp_enqueue_script( 'fitvids', get_template_directory_uri() . '/assets/js/vendor/jquery.fitvids.js', array( 'jquery' ) );
		wp_enqueue_script( 'fluidvids', get_template_directory_uri() . '/assets/js/vendor/fluidvids.js', array( 'jquery' ) );
//	}

	wp_localize_script( 'spawn', 'SpawnAjax', array(
		'postType' => get_query_var( 'post_type' ),
 		'ajaxurl' => admin_url( 'admin-ajax.php' )
	));

 }
 add_action( 'wp_enqueue_scripts', 'spawn_scripts_styles' );

/**
 * Load admin scripts
 */
function spawn_admin_scripts_styles() {
	$postfix = ( defined( 'SCRIPT_DEBUG' ) && true === SCRIPT_DEBUG ) ? '' : '.min';
	wp_enqueue_script( 'spawn-js-admin', get_template_directory_uri() . "/assets/js/spawn_ideas_admin{$postfix}.js", array('jquery'), SPAWN_VERSION, true );

	$screen = get_current_screen();

	if( 'edit-tags' == $screen->base  ){
		wp_enqueue_media();
	}

}

add_action( 'admin_enqueue_scripts', 'spawn_admin_scripts_styles');


 /**
  * Add humans.txt to the <head> element.
  */
 function spawn_header_meta() {
	$humans = '<link type="text/plain" rel="author" href="' . get_template_directory_uri() . '/humans.txt" />';

	echo apply_filters( 'spawn_humans', $humans );
 }
 add_action( 'wp_head', 'spawn_header_meta' );

/**
 * Set the content width based on the theme's design and stylesheet.
 */
if ( ! isset( $content_width ) ) {
	$content_width = 640; /* pixels */
}

/**
 * Register widget area.
 *
 * @link http://codex.wordpress.org/Function_Reference/register_sidebar
 */
function spawn_widgets_init() {
	register_sidebar( array(
		'name'          => __( 'Sidebar', 'spawn' ),
		'id'            => 'sidebar-1',
		'description'   => '',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h1 class="widget-title">',
		'after_title'   => '</h1>',
	) );
	
	register_sidebar( array(
		'name'          => __( 'Award Logos', 'spawn' ),
		'id'            => 'award-logos',
		'description'   => 'Place award logos here to appear on the about page',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h1 class="widget-title">',
		'after_title'   => '</h1>',
	) );
}
add_action( 'widgets_init', 'spawn_widgets_init' );

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/includes/template-tags.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/includes/extras.php';

/**
 * Load Carrington Build files.
 */
require get_template_directory() . '/includes/carrington-build/carrington-build.php';

/**
 * Load Post Types & Taxonomies
 */
require get_template_directory() . '/includes/class-spawn-projects.php';
require get_template_directory() . '/includes/class-spawn-jobs.php';
require get_template_directory() . '/includes/class-spawn-staff.php';
require get_template_directory() . '/includes/class-spawn-brands.php';
require get_template_directory() . '/includes/class-taxonomy.php';
require get_template_directory() . '/includes/class-instagram-widget.php';
require get_template_directory() . '/includes/class-featured-video-meta.php';

/**
 * Load the Theme Customizer
 */
require get_template_directory() . '/includes/theme-customizer.php';

/**
 * SwiftStream Lazy load images.
 */
require get_template_directory() . '/includes/swiftstream/SwiftStream.php';

/**
 * Adjust posts per page for front page only
 */

function spawn_posts_per_page() {
	if ( is_tax( 'industries' ) && WP_Query::is_main_query() ) {
		set_query_var( 'posts_per_page', 4 );
	}
	elseif ( is_post_type_archive( 'spawn-service' ) && WP_Query::is_main_query() ) {
		set_query_var( 'posts_per_page', -1 );
	}

}
add_action( 'pre_get_posts', 'spawn_posts_per_page' );

/**
 * Adjust excerpts
 */

function spawn_excerpt_length( $length ) {
	return 80;
}
add_filter( 'excerpt_length', 'spawn_excerpt_length', 20 );


//Read More Button For Excerpt
function spawn_continue_reading_link($output) {
    global $post;
    return $output  .' <p><a class="read-more" href="'. get_permalink( get_the_ID() ) . '">Read More &rarr;</a></p>';
}

add_filter( 'the_excerpt', 'spawn_continue_reading_link' );

function spawn_excerpt_more($more){
	$more = ' &hellip;';
	return $more;
}
add_filter('excerpt_more', 'spawn_excerpt_more');

/**
 * Specify which post types get Carrington Build
 */

function spawn_build_admin_filter($types) {
	unset( $types );
	$types = array('spawn-projects');
	return $types;
}
add_filter('cfct-build-enabled-post-types', 'spawn_build_admin_filter');

/**
 * Deregister carrington carousel module
 */
function spawn_cfct_deregister_modules() {
    cfct_build_deregister_module('cfct_module_carousel');
}
add_action('cfct-modules-included', 'spawn_cfct_deregister_modules', 99);


add_action( 'wp_ajax_nopriv_load_filter', 'load_filter' );
add_action( 'wp_ajax_load_filter', 'load_filter' );
function load_filter() {
	global $wpdb;
    $cat_id = $_POST[ 'cat' ];
    if ( 'spawn-projects' == $_POST[ 'postType' ] ) {
    	$post_type = 'spawn-projects';
    } else {
    	$post_type = 'post';
    }
    $args = array (
        'post_type' => $post_type,
        'posts_per_page' => -1,
        'tax_query' => array(
        	'relation' => 'OR',
			array (
				'taxonomy' => 'category',
				'field' => 'term_id',
				'terms' => $cat_id
			),
			array (
				'taxonomy' => 'keywords',
				'field' => 'term_id',
				'terms' => $cat_id
			),
			array (
				'taxonomy' => 'services',
				'field' => 'term_id',
				'terms' => $cat_id
			),
		)
    );

    $posts = get_posts( $args );
    global $post;

ob_start ();

    foreach ( $posts as $post ) {
    setup_postdata( $post );
    if ( $post_type == 'spawn-projects' ) {
    	get_template_part('content', 'grid');
    } else {
    	get_template_part('content', 'list');
    }
   }
   wp_reset_postdata();

   $response = ob_get_contents();
   ob_end_clean();

   echo $response;
	die();
}

// Add a second featured image for Staff bios

if (class_exists('MultiPostThumbnails')) {
    new MultiPostThumbnails(
        array(
            'label' => 'Secondary Image',
            'id' => 'secondary-image',
            'post_type' => 'spawn-staff'
        )
    );
    new MultiPostThumbnails(
        array(
            'label' => 'Secondary Image',
            'id' => 'secondary-image',
            'post_type' => 'spawn-projects'
        )
    );
}


/**
 * Adds a checkbox to the featured image metabox.
 *
 * @param string $content
 */
  
function spawn_featured_image_meta( $content ) {
    global $post;
    $text = __( 'Don\'t display image in post.', 'prefix' );
    $id = 'hide_featured_image';
    $value = esc_attr( get_post_meta( $post->ID, $id, true ) );
    $label = '<label for="' . $id . '" class="selectit"><input name="' . $id . '" type="checkbox" id="' . $id . '" value="' . $value . ' "'. checked( $value, 1, false) .'> ' . $text .'</label>';
    return $content .= $label;
}
add_filter( 'admin_post_thumbnail_html', 'spawn_featured_image_meta' );


/**
 * Save featured image meta data when saved
 *
 * @param int $post_id The ID of the post.
 * @param post $post the post.
 */
function spawn_save_featured_image_meta( $post_id, $post, $update ) {
  
    $value = 0;
    if ( isset( $_REQUEST['hide_featured_image'] ) ) {
        $value = 1;
    }
 
    // Set meta value to either 1 or 0
    update_post_meta( $post_id, 'hide_featured_image', $value );
  
}
add_action( 'save_post', 'spawn_save_featured_image_meta', 10, 3 );


// Make custom image sizes selectable in the media manager

function spawn_custom_sizes( $sizes ) {
    return array_merge( $sizes, array(
		'square' => 'Mid square (300x300)',
		'square-large' => 'Large square (600x600)',
		'full-width' => 'Banner (1200x300)',
		'half-width' => 'Half width banner (600x320)',
		'blog-column' => 'Blog Image (720px wide)'
    ) );
}
add_filter( 'image_size_names_choose', 'spawn_custom_sizes' );

// Limit category archives to 3 posts per page

function spawn_limit_category_posts( $query ){
	if( is_category() && $query->is_main_query() ) {
		$query->set('posts_per_page', 3);
		return;
	}
	if( is_post_type_archive( 'spawn-projects' ) && $query->is_main_query() ) {
		$query->set('posts_per_page', -1);
		return;
	}

}
add_action('pre_get_posts', 'spawn_limit_category_posts');

/* Add a featured image to categories */

// Add term page
function spawn_taxonomy_add_new_meta_field() {
	// this will add the custom meta field to the add new term page
	?>
	<div class="form-field">
		<label for="tax_image">Category logo</label>
			<div id="asset"></div>
		<input type="text" name="tax_image" id="tax_image" value="" size="40">
		<a href="" class="button upload_taxonomy_image" id="tax_image_upload_button" ref="image">Upload Image</a>
		<p>Maximum display size: 600 x 300 pixels</p>
	</div>

	<div class="form-field">
		<label for="tax_image_id"> </label>
		<input type="hidden" name="tax_image_id" id="tax_image_id" value="">
	</div>
<?php
}
add_action( 'category_add_form_fields', 'spawn_taxonomy_add_new_meta_field', 10, 2 );

// Edit term page
function spawn_taxonomy_edit_meta_field($term) {
// this will add the custom meta field to the edit term page
?>
	<table class="form-table term-meta-box">
		<tr>
			<th>
				<label for="tax_image">Category Image</label>
			</th>
			<td>
				<div id="asset"></div>
				<input type="text" name="tax_image" id="tax_image" value="" size="40">
				<a href="" class="button upload_taxonomy_image" id="tax_image_upload_button" ref="image">Upload Image</a>
				<p>Displayed at 600px wide</p>
			</td>
		</tr>
	</table>

	<table class="form-table term-meta-box">
		<tr>
			<th>
				<label for="tax_image_id"></label>
			</th>
			<td>
				<input type="hidden" name="tax_image_id" id="tax_image_id" value="">
			</td>
		</tr>
	</table>
<?php
}
add_action( 'category_edit_form_fields', 'spawn_taxonomy_edit_meta_field', 10, 2 );

// Save extra taxonomy fields callback function.
function spawn_save_taxonomy_custom_meta( $term_id ) {
	$term_meta = get_option( 'category_meta' );
	$meta_data = array();

	if ( isset( $_POST[ 'tax_image' ] ) && !empty( $_POST[ 'tax_image' ] ) ) {
		$meta_data[ 'tax_image'  ] = sanitize_text_field( $_POST[ 'tax_image' ] );
	}

	if ( isset( $_POST[ 'tax_image_id' ] ) && !empty( $_POST[ 'tax_image_id' ] ) ) {
		$meta_data[ 'tax_image_id'  ] = sanitize_text_field( $_POST[ 'tax_image_id' ] );
	}

	if ( ! empty( $meta_data ) ) {
		$term_meta[$term_id] = $meta_data;
		update_option( 'category_meta', $term_meta );
	}
}
add_action( 'edited_category', 'spawn_save_taxonomy_custom_meta', 10, 2 );
add_action( 'create_category', 'spawn_save_taxonomy_custom_meta', 10, 2 );

// Delete category meta on category delete
function spawn_delete_taxonomy_custom_data( $term_id ) {
	$term_meta = get_option( 'category_meta' );

	if( isset( $term_meta[ $term_id ] ) ){
		unset(  $term_meta[ $term_id ] );
		update_option( 'category_meta', $term_meta );
	}
}
add_action( 'delete_category', 'spawn_delete_taxonomy_custom_meta' );


function spawn_terms_clauses( $clauses, $taxonomy, $args ) {
	global $wpdb;

	if ( isset( $args['post_type'] ) ) {
		$clauses['join'] .= " INNER JOIN {$wpdb->term_relationships} AS r ON r.term_taxonomy_id = tt.term_taxonomy_id INNER JOIN {$wpdb->posts} AS p ON p.ID = r.object_id";
		$clauses['where'] .= $wpdb->prepare( " AND p.post_type='%s'", $args['post_type'] );
		$clauses['orderby'] = ' GROUP BY t.term_id ' . $clauses['orderby'];
	}

	return $clauses;
}
add_filter( 'terms_clauses', 'spawn_terms_clauses', 10, 3 );

add_action( 'init', 'spawn_default_post_type', 1 );
function spawn_default_post_type() {
 
    register_post_type( 'post', array(
        'labels' => array(
            'name_admin_bar' => _x( 'Post', 'add new on admin bar' ),
        ),
        'public'  => true,
        '_builtin' => false, 
        '_edit_link' => 'post.php?post=%d', 
        'capability_type' => 'post',
        'map_meta_cap' => true,
        'hierarchical' => false,
        'rewrite' => array( 'slug' => 'blog' ),
        'query_var' => false,
        'supports' => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'trackbacks', 'custom-fields', 'comments', 'revisions', 'post-formats' ),
    ) );
}


