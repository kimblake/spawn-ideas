<?php
/**
 * @package spawn
 */
?>
<div class="row content-job">
	<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
		<div class="entry">
			<header class="entry-header">
				<h3 class="entry-title"><a href="<?php the_permalink(); ?>" rel="bookmark"><?php the_title() ?></a></h3>
			</header><!-- .entry-header -->
			<div class="entry-content">
				<?php the_excerpt(); ?>
			</div>
		</div>

		<div class="apply-footer">
			<a href="<?php the_permalink(); ?>" class="button apply">
				<?php esc_html_e( 'Apply', 'spawn' ); ?>
			</a>
		</div>
	</article><!-- #post-## -->
</div>