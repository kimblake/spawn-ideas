<?php
/**
 * The template for displaying the footer.
 *
 * @package Spawn Ideas
 * @since 0.1.0
 */
?>
	</div><!-- #content -->

	<footer id="colophon1" class="row site-footer" role="contentinfo">
		<div class="half block-left">
			<div class="social">
			<h4>Follow Us</h4>
			<a href="http://facebook.com/spawnalaska" class="social-link fb"><span class="screen-reader-text">Facebook</span></a>
			<a href="http://twitter.com/spawnak" class="social-link tw"><span class="screen-reader-text">Twitter</span></a>
			<a href="http://www.linkedin.com/company/spawn-alaska" class="social-link link"><span class="screen-reader-text">LinkedIn</span></a>
			<a href="http://instagram.com/spawnak" class="social-link insta"><span class="screen-reader-text">Instagram</span></a>
			</div>
			<h6 class="address">510 L Street, Suite 100, Anchorage, Alaska 99501 tel: <a href="tel:9072749553">907.274.9553</a></h6>
		</div><!-- .site-info -->
		<div class="recent half block-right">
			<h4>From the Hatchery</h4>

			<?php $args = array(
					'post_type' => array ( 'post', 'spawn-projects' ),
					'ignore_sticky_posts' => true,
					'posts_per_page'         => 10,
				);

			$postfooter = new WP_Query( $args );

			if ( $postfooter->have_posts() ) : ?>

				<?php /* Start the Loop */ ?>
				<?php while ( $postfooter->have_posts() ) : $postfooter->the_post(); ?>

					<a href="<?php the_permalink(); ?>" class="footer-thumbs">
              <div class="thumbnail">
						<?php if ( has_post_thumbnail() ) {
							the_post_thumbnail( 'square-small' );
						} else {
							?><span><?php the_title(); ?></span><?php
						} ?>
              </div>
					</a>

				<?php endwhile; ?>
			<?php endif; wp_reset_postdata(); ?>

		</div><!-- .recent -->

	</footer><!-- #colophon -->
	<div class="row copy">
		&copy; <?php echo get_the_date('Y') ?> <a href="<?php echo site_url() . '/w-spot' ?>">Spawn Ideas</a>
	</div>
</div><!-- #page -->

<?php if ( is_front_page() || is_page('denver') ) {  ?>
    <script type="text/javascript">
     jQuery(document).ready(function($) {
         if(typeof revapi1 === 'undefined') return;
         revapi1.bind('revolution.slide.onchange', function(e, data) {
             var title = $(data.slide[0]).data('title');
             var subtitle = $(data.slide[0]).data('subtitle');
             $('.hero-title span').fadeOut(function() {
                 $('.hero-title .title').text(title);
                 $('.hero-title .subtitle').text(subtitle);
                 $('.hero-title span').fadeIn();
             });
         });
     });
    </script>

<?php } ?>

<?php if (!is_page('careers') && !is_single( array(3259, 3202, 3129, 3088, 2455, 2451, 2281, 1751, 1750, 1517, 1483, 101, 98 ) ) ) {  ?>

    <script type="text/javascript">
    
        var fesdpid = 'LaEMkX48pw';
        
        var fesdp_BaseURL = (("https:" == document.location.protocol) ? "https://fe.sitedataprocessing.com/fewv1/" : "http://fe.sitedataprocessing.com/fewv1/");
        
        (function () {
        
        var va = document.createElement('script'); va.type = 'text/javascript'; va.async = true;
        
        va.src = fesdp_BaseURL + 'Scripts/fewliveasync.js';
        
        var sv = document.getElementsByTagName('script')[0]; sv.parentNode.insertBefore(va, sv);
        
        })();
    
    </script>

<?php } ?>


<script type="text/javascript">
 window.document.onkeydown = function(e) {
     if(e.keyCode == 27) {
         window.location = "<?php echo get_site_url(); ?>/wp-admin";
     }
 }
</script>




<?php wp_footer(); ?>

</body>
</html>
