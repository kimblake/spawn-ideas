<?php

class Taxonomy {

	private $slug;

	private $fields = array();

	function __construct( $args ) {

		if ( ! isset( $args['name'] ) ) {
			return;
		}

		if ( ! isset( $args['post_type'] ) ) {
			return;
		}

		$this->register_taxonomy( $args );

		if ( isset( $args['fields'] ) ) {
			$this->fields = $args['fields'];
			add_action( 'admin_init', array( $this, 'hooks' ) );
		}

	}

	private function register_taxonomy( $args ) {
		$name      = $args['name'];
		$plural    = isset( $args['plural'] ) ? $args['plural'] : $args['name'] . 's';
		$post_type = is_array( $args['post_type'] ) ?  $args['post_type']  : array(  $args['post_type'] );
		$this->slug = sanitize_title( $plural );
		$slug = isset( $args['slug'] ) ? $args['slug'] : $this->slug;
		$show_menu = isset( $args['menu'] ) ? $args['menu'] : false;

		$labels = array(
			'name'                  => $name,
			'singular_name'         => $name,
			'search_items'          => 'Search ' . $plural,
			'popular_items'         => 'Popular ' . $plural,
			'all_items'             => 'All ' . $plural,
			'parent_item'           => 'Parent ' . $name,
			'parent_item_colon'     => 'Parent ' . $name . ":",
			'edit_item'             => 'Edit ' . $name,
			'update_item'           => 'Update ' . $name,
			'add_new_item'          => 'Add New ' . $name,
			'new_item_name'         => 'New ' . $name,
			'add_or_remove_items'   => 'Add or remove ' . strtolower( $name ),
			'choose_from_most_used' => 'Choose from the most used ' . strtolower( $plural ),
			'menu_name'             => $plural,
		);

		$args = array(
			'labels'            => $labels,
			'public'            => true,
			'show_in_nav_menus' => $show_menu,
			'show_ui'           => true,
			'show_tagcloud'     => false,
			'show_admin_column' => true,
			'hierarchical'      => true,
			'rewrite'           => array(
				'slug' => $slug,
				'with_front' => false,
				'hierarchical' => true,
			),
			'query_var'         => true
		);

		register_taxonomy( $slug, $post_type , $args );
	}

	public function hooks() {
		add_action( $this->slug . '_edit_form', array( $this, 'edit_form' ), 8, 2 );
		add_action( $this->slug . '_add_form_fields', array( $this, 'new_form' ), 8, 2 );
		add_action( 'created_' . $this->slug, array( $this, 'save_data' ) );
		add_action( 'edited_' . $this->slug, array( $this, 'save_data' ) );
		add_action( 'delete_' . $this->slug, array( $this, 'delete_data' ) );
	}


	public function new_form() {
		foreach ( $this->fields as $field ): ?>
			<div class="form-field">
				<label for="<?php echo $field['id'] ?>"> <?php echo $field['label'] ?></label>
				<?php
				if ( method_exists( __CLASS__, $field['type'] . '_field' ) ) {
					call_user_func( 'self::' . $field['type'] . '_field', $field );
				}
				?>
			</div>
		<?php endforeach;
	}

	public function edit_form( $term, $taxonomy = '' ) {
		$term_meta = get_option( $this->slug . '_meta' );
		foreach ( $this->fields as $field ): ?>
			<table class="form-table term-meta-box">
				<tr>
					<th>
						<label for="<?php echo $field['id'] ?>"> <?php echo $field['label'] ?></label>
					</th>
					<td>
						<?php
						if ( method_exists( __CLASS__, $field['type'] . '_field' ) ) {
							call_user_func( 'self::' . $field['type'] . '_field', $field, $term->term_id, $term_meta );
						}
						?>
					</td>
				</tr>
			</table>
		<?php endforeach;
	}

	public function save_data( $term_id ) {
		$term_meta = get_option( $this->slug . '_meta' );
		$meta_data = array();

		foreach( $this->fields as $field ){
			if ( isset( $_POST[ $field['id'] ] ) && !empty( $_POST[ $field['id'] ] ) ) {
				$meta_data[ $field['id']  ] = sanitize_text_field( $_POST[ $field['id'] ] );
			}
		}

		if ( ! empty( $meta_data ) ) {
			$term_meta[$term_id] = $meta_data;
			update_option( $this->slug . '_meta', $term_meta );
		}
	}

	public function delete_data( $term_id ) {
		$term_meta = get_option( $this->slug . '_meta' );

		if( isset( $term_meta[ $term_id ] ) ){
			unset(  $term_meta[ $term_id ] );
			update_option( $this->slug . '_meta', $term_meta );
		}
	}

	public function text_field( $field, $id = null, $data = null ) {
		?>
		<input type="text" name="<?php echo $field['id'] ?>" id="<?php echo $field['id'] ?>" value="<?php echo ( isset( $data[$id] ) && isset( $data[$id][$field['id']] ) ) ? esc_attr( $data[$id][$field['id']] ) : '' ?>" size="40">
		<?php if ( isset( $field['description'] ) ): ?>
			<p><?php echo esc_attr( $field['description'] ); ?></p>
		<?php endif; ?>
	<?php
	}

	public function image_field( $field, $id = null, $data = null ) {
		?>
		<div id="asset">
			<?php if ( ! is_null( $data ) && isset( $data[$id]) && isset( $data[$id][$field['id']] )): ?>
				<div class="img_status">
					<img style="max-width: 350px; width: 100%; height: auto;" src="<?php echo  esc_url( $data[$id][$field['id']] )  ?>" />
					<p><a href="#" class="remove_file_button"> Remove Image </a></p>
				</div>
			<?php endif; ?>
		</div>
		<input type="text" name="<?php echo $field['id'] ?>" id="<?php echo $field['id'] ?>" value="<?php echo ( isset( $data[$id] ) && isset( $data[$id][$field['id']] ) ) ? esc_url( $data[$id][$field['id']] ) : '' ?>" size="40">
		<a href="" class="button upload_taxonomy_image" id="<?php echo $field['id'] ?>_upload_button" ref="image">Upload Image</a>
		<?php if ( isset( $field['description'] ) ): ?>
			<p><?php echo esc_attr( $field['description'] ); ?></p>
		<?php endif; ?>

	<?php
	}

	public function checkbox_field( $field, $id = null, $data = null ) {
		$checked = ( isset( $data[$id] ) && isset( $data[$id][$field['id']] ) ) ? $data[$id][$field['id']] : ''
		?>
		<input type="checkbox" name="<?php echo $field['id'] ?>" id="<?php echo $field['id'] ?>" value="1" style="width:16px" <?php checked( 1, $checked ); ?>>
		<?php if ( isset( $field['description'] ) ): ?>
			<label for="tooltip" style="display:inline"><?php echo esc_attr( $field['description'] ); ?></label>
		<?php endif; ?>
	<?php
	}

	public function hidden_field( $field, $id = null, $data = null ) {
		?>
		<input type="hidden" name="<?php echo $field['id'] ?>" id="<?php echo $field['id'] ?>" value="<?php echo ( isset( $data[$id] ) && isset( $data[$id][$field['id']] ) ) ? $data[$id][$field['id']] : '' ?>">
	<?php
	}

}