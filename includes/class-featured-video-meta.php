<?php

/**
 * Add a featured video option to pages
 */
class Spawn_Featured_Video {
    private static $_instance;

    /**
     * Setup actions and filters. This is a singleton.
     *
     * @since 0.1
     * @uses add_action, add_filter
     */
    private function __construct() {
        add_action( 'add_meta_boxes', array( $this, 'action_add_meta_boxes' ) );
        add_action( 'save_post', array( $this, 'action_save_post' ) );
    }

    /**
     * Output meta box
     *
     * @param object $post
     * @return void
     */
    public function meta_box_options( $post ) {
        wp_nonce_field( 'spawn_featured_video_options_action', 'spawn_featured_video_options' );

        $url = get_post_meta( $post->ID, 'spawn_featured_video_url', true );
        if ( ! empty( $url ) )
            $url = esc_url( $url );

        ?>
        <p>
            <label for="spawn_featured_video_url">Youtube or Vimeo URL:</label>
            <input class="regular-text" type="text" name="spawn_featured_video_url" id="spawn_featured_video_url" value="<?php echo $url; ?>">
        </p>

    <?php
    }

    /**
     * Save information associated with metabox
     *
     * @param int $post_id
     * @since 0.1
     * @return void
     */
    public function action_save_post( $post_id ) {
        if ( ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) || ! current_user_can( 'edit_post', $post_id ) || 'revision' == get_post_type( $post_id ) )
            return;

        if ( ! empty( $_POST['spawn_featured_video_options'] ) && wp_verify_nonce( $_POST['spawn_featured_video_options'], 'spawn_featured_video_options_action' ) ) {

            if ( ! empty( $_POST['spawn_featured_video_url'] ) ) {
                update_post_meta( $post_id, 'spawn_featured_video_url', esc_url_raw( $_POST['spawn_featured_video_url'] ) );
            } else {
                delete_post_meta( $post_id, 'spawn_featured_video_url' );
            }

        }

    }

    /**
     * Register metaboxes
     *
     * @uses add_meta_box, __
     * @return void
     */
    public function action_add_meta_boxes() {
        add_meta_box( 'spawn_featured_video', 'Featured Video', array( $this, 'meta_box_options' ), 'page' );
    }

    /**
     * Initialize class and return an instance of it
     *
     * @return object
     */
    public function init() {
        if ( ! isset( self::$_instance ) ) {

            self::$_instance = new self();
        }

        return self::$_instance;
    }
}

Spawn_Featured_Video::init();
