<?php

/**
 * Spawn Portfolio Staff Post Type
 */
class Spawn_Staff {
	private static $_instance;

	/**
	 * Setup actions and filters. This is a singleton.
	 *
	 * @since 0.1
	 * @uses add_action, add_filter
	 */
	private function __construct() {
		add_action( 'init', array( $this, 'action_register_cpt' ) );
		add_action( 'init', array( $this, 'action_register_tax' ) );
		add_action( 'save_post', array( $this, 'action_save_post' ) );
		add_filter( 'post_updated_messages', array( $this, 'filter_post_updated_messages' ) );
	}

	/**
	 * Registers post type for providers
	 *
	 * @since 0.1
	 * @return void
	 */
	public function action_register_cpt() {
		$labels = array(
			'name'               => 'Staff',
			'singular_name'      => 'Staff',
			'add_new'            => 'Add New',
			'add_new_item'       => 'Add New Staff Member',
			'edit_item'          => 'Edit Staff',
			'new_item'           => 'New Staff',
			'all_items'          => 'All Staff',
			'view_item'          => 'View Staff',
			'search_items'       => 'Search Staff',
			'not_found'          => 'No staff found',
			'not_found_in_trash' => 'No staff found in trash',
			'parent_item_colon'  => '',
			'menu_name'          => 'Staff',
		);

		$args = array(
			'labels'                 => $labels,
			'public'                 => true,
			'publicly_queryable'     => true,
			'show_ui'                => true,
			'show_in_menu'           => true,
			'query_var'              => true,
			'rewrite'                => array( 'slug' => 'staff' ),
			'has_archive'            => false,
			'hierarchical'           => true,
			'menu_position'          => null,
			'with_front'             => false,
			'supports'               => array( 'title', 'thumbnail', 'editor' ),
			'register_meta_box_cb'   => array( $this, 'add_meta_boxes' ),
		);

		register_post_type( 'spawn-staff', $args );
	}

	/**
	 * Filter CPT messages
	 *
	 * @param array $messages
	 * @uses get_permalink, esc_url, wp_post_revision_title, __, add_query_arg
	 * @return array
	 */
	public function filter_post_updated_messages( $messages ) {
		global $post, $post_ID;

		$messages['spawn-staff'] = array(
			0 => '', // Unused. Messages start at index 1.
			1 => sprintf( 'Staff post updated. <a href="%s">View post</a>', esc_url( get_permalink( $post_ID ) ) ),
			2 => 'Custom field updated.',
			3 => 'Custom field deleted.',
			4 => 'Staff post updated',
			5 => isset( $_GET['revision']) ? sprintf( ' Staff post restored to revision from %s', wp_post_revision_title( (int) $_GET['revision'], false ) ) : false,
			6 => sprintf( 'Staff post published. <a href="%s">View Post</a>', esc_url( get_permalink( $post_ID) ) ),
			7 => 'Staff post saved',
			8 => sprintf( 'Staff post submitted. <a target="_blank" href="%s">Preview post</a>', esc_url( add_query_arg( 'preview', 'true', get_permalink( $post_ID ) ) ) ),
			9 => sprintf( 'Staff post scheduled for: <strong>%1$s</strong>. <a target="_blank" href="%2$s">Preview post</a>',
				date_i18n( 'M j, Y @ G:i', strtotime( $post->post_date ) ), esc_url( get_permalink( $post_ID ) ) ),
			10 => sprintf( 'Staff draft updated. <a target="_blank" href="%s">Preview post</a>', esc_url( add_query_arg( 'preview', 'true', get_permalink( $post_ID ) ) ) ),
		);

		return $messages;
	}

	/**
	 * Registers taxonomies for projects
	 *
	 * @since 0.1
	 * @return void
	 */
	public function action_register_tax() {
		$leadership_args = array(
			'hierarchical' => false,
			'show_ui' => false,
			'show_admin_column' => false,
			'query_var' => false,
			'rewrite' => false,
		);

		$contact_args = array(
			'hierarchical' => false,
			'show_ui' => false,
			'show_admin_column' => false,
			'query_var' => false,
			'rewrite' => false,
		);

		register_taxonomy( 'spawn-staff-leadership', array( 'spawn-staff' ), $leadership_args );
		register_taxonomy( 'spawn-staff-contact', array( 'spawn-staff' ), $contact_args );

	}


	/**
	 * Register metaboxes
	 *
	 * @uses add_meta_box
	 * @return void
	 */
	public function add_meta_boxes() {
		add_meta_box( 'spawn_staff_details', 'Staff Details', array( $this, 'meta_box_options' ), 'spawn-staff', 'normal', 'high' );
		add_meta_box( 'spawn_job_leadership', 'Display in Leadership team?', array( $this, 'featured_meta_box_options' ), 'spawn-staff', 'side', 'core' );
		add_meta_box( 'spawn_job_contact', 'Is this a contact person?', array( $this, 'contact_meta_box_options' ), 'spawn-staff', 'side', 'core' );
	}

	/**
	 * Output Staff details meta box
	 *
	 * @param object $post
	 * @return void
	 */
	public function meta_box_options( $post ) {
		wp_nonce_field( 'spawn_staff_details_action', 'spawn_staff_details' );

		$title = get_post_meta( $post->ID, 'spawn_staff_title', true );

		$email = get_post_meta( $post->ID, 'spawn_staff_email', true );

		$education = get_post_meta( $post->ID, 'spawn_staff_education', true );

		$bravest = get_post_meta( $post->ID, 'spawn_staff_bravest', true );

		?>
		<p>
			<label for="spawn_staff_title">Job Title:</label>
			<input class="regular-text" type="text" name="spawn_staff_title" id="spawn_staff_title" value="<?php echo esc_attr( $title ); ?>">
		</p>

		<p>
			<label for="spawn_staff_email">Email:</label>
			<input class="regular-text" type="text" name="spawn_staff_email" id="spawn_staff_email" value="<?php echo esc_attr( $email ); ?>">
		</p>

		<p>
			<label for="spawn_staff_education">Education:</label>
			<input class="regular-text" type="text" name="spawn_staff_education" id="spawn_staff_education" value="<?php echo esc_attr( $education ); ?>">
		</p>

		<p>
			<label for="spawn_staff_bravest">Bravest Moment:</label>
			<input class="regular-text" type="text" name="spawn_staff_bravest" id="spawn_staff_bravest" value="<?php echo esc_attr( $bravest ); ?>">
		</p>


	<?php
	}

	/**
	 * Display a meta box for the active checkbox
	 * @param  object $post
	 * @return void
	 */
	function featured_meta_box_options( $post ) {
		wp_nonce_field( 'additional_options_action', 'additional_options' );
		$featured = has_term( 'staff-leadership', 'spawn-staff-leadership', $post );
		echo 'Leadership: <input type="checkbox" name="staff-leadership" value="1" ' . ( ( $featured ) ? 'checked="checked"' : '' ) . '>';
	}

	/**
	 * Display a meta box for the active checkbox
	 * @param  object $post
	 * @return void
	 */
	function contact_meta_box_options( $post ) {
		wp_nonce_field( 'contact_options_action', 'contact_options' );
		$contact = has_term( 'staff-contact', 'spawn-staff-contact', $post );
		$contactarea = get_post_meta( $post->ID, 'spawn_staff_contactarea', true );

		echo 'On contact page: <input type="checkbox" name="staff-contact" value="1" ' . ( ( $contact ) ? 'checked="checked"' : '' ) . '>';
		?>
		<p>
			<label for="spawn_staff_contactarea">Use as a contact for:</label>
			<input class="widefat" type="text" name="spawn_staff_contactarea" id="spawn_staff_contactarea" value="<?php echo esc_attr( $contactarea ); ?>">
		</p>

		<?php
	}


	/**
	 * Save information associated with CPT
	 *
	 * @param int $post_id
	 * @since 0.1
	 * @return void
	 */
	public function action_save_post( $post_id ) {
		if ( ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) || ! current_user_can( 'edit_post', $post_id ) || 'revision' == get_post_type( $post_id ) )
			return;

		if ( ! empty( $_POST['spawn_staff_details'] ) && wp_verify_nonce( $_POST['spawn_staff_details'], 'spawn_staff_details_action' ) ) {

			if ( ! empty( $_POST['spawn_staff_title'] ) ) {
				update_post_meta( $post_id, 'spawn_staff_title', sanitize_text_field( $_POST['spawn_staff_title'] ) );
			} else {
				delete_post_meta( $post_id, 'spawn_staff_title' );
			}

			if ( ! empty( $_POST['spawn_staff_email'] ) ) {
				update_post_meta( $post_id, 'spawn_staff_email', sanitize_text_field( $_POST['spawn_staff_email'] ) );
			} else {
				delete_post_meta( $post_id, 'spawn_staff_email' );
			}

			if ( ! empty( $_POST['spawn_staff_education'] ) ) {
				update_post_meta( $post_id, 'spawn_staff_education', sanitize_text_field( $_POST['spawn_staff_education'] ) );
			} else {
				delete_post_meta( $post_id, 'spawn_staff_education' );
			}

			if ( ! empty( $_POST['spawn_staff_bravest'] ) ) {
				update_post_meta( $post_id, 'spawn_staff_bravest', sanitize_text_field( $_POST['spawn_staff_bravest'] ) );
			} else {
				delete_post_meta( $post_id, 'spawn_staff_bravest' );
			}

		}

		if ( ! empty( $_POST['additional_options'] ) && wp_verify_nonce( $_POST['additional_options'], 'additional_options_action' ) ) {
			if ( ! empty( $_POST['staff-leadership'] ) ) {
				$featured = term_exists( 'staff-leadership', 'spawn-staff-leadership' );
				if ( empty( $featured ) ) {
        			$featured = wp_insert_term( 'staff-leadership', 'spawn-staff-leadership' );
				}
				wp_set_post_terms( $post_id, array( (int) $featured['term_id'] ), 'spawn-staff-leadership' );
			} else {
				wp_set_post_terms( $post_id, array(), 'spawn-staff-leadership' );
			}
		}

		if ( ! empty( $_POST['contact_options'] ) && wp_verify_nonce( $_POST['contact_options'], 'contact_options_action' ) ) {
			if ( ! empty( $_POST['staff-contact'] ) ) {
				$featured = term_exists( 'staff-contact', 'spawn-staff-contact' );
				if ( empty( $featured ) ) {
        			$featured = wp_insert_term( 'staff-contact', 'spawn-staff-contact' );
				}
				wp_set_post_terms( $post_id, array( (int) $featured['term_id'] ), 'spawn-staff-contact' );
			} else {
				wp_set_post_terms( $post_id, array(), 'spawn-staff-contact' );
			}

			if ( ! empty( $_POST['spawn_staff_contactarea'] ) ) {
				update_post_meta( $post_id, 'spawn_staff_contactarea', sanitize_text_field( $_POST['spawn_staff_contactarea'] ) );
			} else {
				delete_post_meta( $post_id, 'spawn_staff_contactarea' );
			}

		}

	}


	/**
	 * Initialize class and return an instance of it
	 *
	 * @return object
	 */
	public static function init() {
		if ( ! isset( self::$_instance ) ) {

			self::$_instance = new self();
		}

		return self::$_instance;
	}
}

Spawn_Staff::init();

