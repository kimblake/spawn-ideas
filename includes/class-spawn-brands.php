<?php

/**
 * Spawn Client/brand post type
 */
class Spawn_Brands {
	private static $_instance;

	/**
	 * Setup actions and filters. This is a singleton.
	 *
	 * @since 0.1
	 * @uses add_action, add_filter
	 */
	private function __construct() {
		add_action( 'init', array( $this, 'action_register_cpt' ) );
		add_action( 'save_post', array( $this, 'action_save_post' ) );
		add_filter( 'post_updated_messages', array( $this, 'filter_post_updated_messages' ) );
		add_action( 'save_post', array( $this, 'update_brand_taxonomy') );
		add_filter( 'manage_posts_columns', array( $this, 'brand_columns_head' ) );
		add_action( 'manage_posts_custom_column', array( $this, 'brand_columns_content'), 10, 2);
	}

	/**
	 * Registers post type for providers
	 *
	 * @since 0.1
	 * @return void
	 */
	public function action_register_cpt() {
		$labels = array(
			'name'               => 'Brands',
			'singular_name'      => 'Brands',
			'add_new'            => 'Add New',
			'add_new_item'       => 'Add New Brand',
			'edit_item'          => 'Edit Brand',
			'new_item'           => 'New Brand',
			'all_items'          => 'All Brands',
			'view_item'          => 'View Brand',
			'search_items'       => 'Search Brands',
			'not_found'          => 'No brands found',
			'not_found_in_trash' => 'No brands found in trash',
			'parent_item_colon'  => '',
			'menu_name'          => 'Brands',
		);

		$args = array(
			'labels'                 => $labels,
			'public'                 => true,
			'publicly_queryable'     => true,
			'show_ui'                => true,
			'show_in_menu'           => true,
			'query_var'              => true,
			'rewrite'                => false,
			'has_archive'            => false,
			'hierarchical'           => true,
			'menu_position'          => null,
			'with_front'             => false,
			'supports'               => array( 'title', 'thumbnail', 'excerpt' ),
			'register_meta_box_cb'   => array( $this, 'add_meta_boxes' ),
		);

		register_post_type( 'spawn-brands', $args );
		register_taxonomy_for_object_type( 'brands', 'spawn-brands' );
		register_taxonomy_for_object_type( 'category', 'spawn-brands' );
	}

	/**
	 * Filter CPT messages
	 *
	 * @param array $messages
	 * @uses get_permalink, esc_url, wp_post_revision_title, __, add_query_arg
	 * @return array
	 */
	public function filter_post_updated_messages( $messages ) {
		global $post, $post_ID;

		$messages['spawn-brands'] = array(
			0 => '', // Unused. Messages start at index 1.
			1 => sprintf( 'Brand post updated. <a href="%s">View post</a>', esc_url( get_permalink( $post_ID ) ) ),
			2 => 'Custom field updated.',
			3 => 'Custom field deleted.',
			4 => 'Brand post updated',
			5 => isset( $_GET['revision']) ? sprintf( ' Brand post restored to revision from %s', wp_post_revision_title( (int) $_GET['revision'], false ) ) : false,
			6 => sprintf( 'Brand post published. <a href="%s">View Post</a>', esc_url( get_permalink( $post_ID) ) ),
			7 => 'Brand post saved',
			8 => sprintf( 'Brand post submitted. <a target="_blank" href="%s">Preview post</a>', esc_url( add_query_arg( 'preview', 'true', get_permalink( $post_ID ) ) ) ),
			9 => sprintf( 'Brand post scheduled for: <strong>%1$s</strong>. <a target="_blank" href="%2$s">Preview post</a>',
				date_i18n( 'M j, Y @ G:i', strtotime( $post->post_date ) ), esc_url( get_permalink( $post_ID ) ) ),
			10 => sprintf( 'Brand draft updated. <a target="_blank" href="%s">Preview post</a>', esc_url( add_query_arg( 'preview', 'true', get_permalink( $post_ID ) ) ) ),
		);

		return $messages;
	}

	/**
	 * Register metaboxes
	 *
	 * @uses add_meta_box
	 * @return void
	 */
	public function add_meta_boxes() {
		add_meta_box( 'spawn_brand_details', 'Brand Details', array( $this, 'meta_box_options' ), 'spawn-brands', 'normal', 'high' );
	}

	/**
	 * Output brand details meta box
	 *
	 * @param object $post
	 * @return void
	 */
	public function meta_box_options( $post ) {
		wp_nonce_field( 'spawn_brand_details_action', 'spawn_brand_details' );

		$url = get_post_meta( $post->ID, 'spawn_brand_url', true );
		if ( ! empty( $url ) )
			$url = esc_url( $url );

		?>

		<p>
			<label for="url">Brand URL:</label>
			<input class="regular-text" type="url" name="spawn_brand_url" id="spawn_brand_url" value="<?php echo esc_attr( $url ); ?>">
		</p>

	<?php
	}

	/**
	 * Save information associated with CPT
	 *
	 * @param int $post_id
	 * @since 0.1
	 * @return void
	 */
	public function action_save_post( $post_id ) {
		if ( ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) || ! current_user_can( 'edit_post', $post_id ) || 'revision' == get_post_type( $post_id ) )
			return;

		if ( ! empty( $_POST['spawn_brand_details'] ) && wp_verify_nonce( $_POST['spawn_brand_details'], 'spawn_brand_details_action' ) ) {

			if ( ! empty( $_POST['spawn_brand_url'] ) ) {
				update_post_meta( $post_id, 'spawn_brand_url', sanitize_text_field( $_POST['spawn_brand_url'] ) );
			} else {
				delete_post_meta( $post_id, 'spawn_brand_url' );
			}

		}

	}


	/**
	 * Automatically create a entry in the brand taxonomy when a post is created.
	 */
	public function update_brand_taxonomy( $post_id ) {
		// If we're running an auto-save, don't create a term
		if ( ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) || ! current_user_can( 'edit_post', $post_id ) || 'auto-draft' == get_post_status( $post_id ) ) {
			return;
		}

		if ( 'spawn-brands' !== get_post_type( $post_id ) ) {
			return;
		}

		$brand = get_post( $post_id );
		if ( null === $brand ) {
			return;
		}

		wp_set_object_terms( $post_id, $brand->post_title, 'brands', true );
	}

	/**
	 * Helper function to get the featured image
	 */
	public function brand_get_featured_image( $post_ID ) {
		$post_thumbnail_id = get_post_thumbnail_id( $post_ID );
		if ( $post_thumbnail_id ) {
			$post_thumbnail_img = wp_get_attachment_image_src( $post_thumbnail_id, 'square-small' );
			return $post_thumbnail_img[0];
		}
	}

	/**
	 * Add a featured image column to the brand edit screen
	 */
	public function brand_columns_head( $defaults ) {
		$defaults[ 'featured_image' ] = 'Featured Image';
	    return $defaults;
	}

	/**
	 * Show the featured image in the edit screen
	 */
	public function brand_columns_content($column_name, $post_ID) {
		if ( $column_name == 'featured_image' ) {
			$post_featured_image = $this->brand_get_featured_image( $post_ID );
			if ( $post_featured_image ) {
				echo '<img src="' . $post_featured_image . '" />';
		    }
	    }
	}

	/**
	 * Initialize class and return an instance of it
	 *
	 * @return object
	 */
	public static function init() {
		if ( ! isset( self::$_instance ) ) {

			self::$_instance = new self();
		}

		return self::$_instance;
	}
}

Spawn_Brands::init();

