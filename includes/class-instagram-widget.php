<?php

/**
 * Functionality supporting the Instagram API and template printing
 */
class MP_Instagram_Widget {
	/**
	 * Dummy constructor
	 */
	public function __construct() {}

	/**
	 * Get Instagram HTML of submitted parameter
	 * Checks transient cache before rebuilding
	 *
	 * @param $parameter  parameter (username or hashtag) to get from instagram
	 *
	 * @return null if bad response/no response/no data object from instagram
	 * @return string HTML string for outputting
	 */
	public function get_instagram_widget( $parameter ) {
		if ( false === ( $instagram_output = get_transient( 'mp_instagram_widget' ) ) ) {
			$instagram_output = $this->get_instagram_widget_output( $parameter );
			if ( false === $instagram_output )
				return;

			set_transient( 'mp_instagram_widget', $instagram_output, 1800 ); // expire every 30 minutes
		}

		return $instagram_output;
	}

	/**
	 * Gets the output (builds output for the instagram widget)
	 * @param $parameter  string
	 *
	 * @return bool return false if bad response from instagram
	 * @return object return object data if good response
	 */
	protected function get_instagram_widget_output( $parameter ) {
		$instagram_data = $this->get_instagram_data( $parameter );
		if ( false === $instagram_data )
			return false;

		$output = $this->get_instagram_widget_template( $instagram_data, $parameter );
		if ( false === $output )
			return false;

		return $output;
	}

	/**
	 * Get data from Instagram
	 * Uses Easy Instagram to build access token - will not work without Easy Instagram plugin
	 * http://wordpress.org/plugins/easy-instagram/
	 * Must first authenticate with Instagram - follow plugin instructions to obtain access token
	 *
	 * @param $parameter string username or hashtag
	 *
	 * @return bool false if no access token
	 * @return bool false if bad response from Instagram
	 *
	 * @return object image data containing URLs and links from Instagram
	 */
	public function get_instagram_data( $parameter, $type = null ) {
		$access_token = get_option( 'easy_instagram_access_token' );
		if ( false === $access_token )
			return false;

		if ( null == $type )
			$instagram_url = $this->get_instagram_url( $parameter );
		else if ( 'user_search' == $type )
			$instagram_url = "https://api.instagram.com/v1/users/search?q={$parameter}&count=1&";

		if ( false === $instagram_url )
			return false;

		$the_body = wp_remote_get( $instagram_url . 'access_token=' . $access_token );
		$the_body = wp_remote_retrieve_body( $the_body );

		$decoded_response = json_decode( $the_body );
		$instagram_data = $decoded_response->data;

		return $instagram_data;
	}

	/**
	 * Determine Instagram API call (are we looking for a user or hashtag?)
	 */
	protected function get_instagram_url( $parameter ) {
		$preg_match = preg_match( '/^(@|#)([a-z]+)/', $parameter, $matches );
		if ( ! empty( $preg_match ) ) {
			if ( '@' == $matches[1] ) {
				$userID = $this->get_instagram_data( $matches[2], 'user_search' );
				$instagram_url = "https://api.instagram.com/v1/users/{$userID[0]->id}/media/recent?";
			} else if ( '#' == $matches[1] ) {
				$instagram_url = "https://api.instagram.com/v1/tags/{$matches[2]}/media/recent?";
			}
		} else {
			return false;
		}

		return $instagram_url;
	}

	/**
	 * Determine grid size based on number of images returned
	 *
	 * @param $instagram_data
	 *
	 * @return bool false if no images
	 * @return int  grid size
	 */
	protected function calculate_grid( $count = 0 ) {
		$number = sqrt( count( $count ) );
//		if ( is_float( $number ) )
//			return intval( $number );
//		return $number;
		return 6;
	}

	/**
	 * Build HTML from instagram object data
	 * @param $instagram_data
	 *
	 * @return bool false if grid returned false
	 * @return string html output
	 */
	protected function get_instagram_widget_template( $instagram_data, $parameter ) {
		$grid = $this->calculate_grid( $instagram_data );
		if ( false === $grid )
			return false;

		if ( is_array( $instagram_data ) ) {

			$output = "<div class='mp-widget instagram-widget'>\n";

//			$output .= "<h4>{$parameter}</h4>";

			$i = 1;
			foreach ( $instagram_data as $image ) {
				// only allow the right amount to make a perfect grid of squares (4*4, 3*3, 2*2, 1*1)
				if ( $i > $grid )
					break;

				if ( $i % $grid == 0 )
					$last = 'last';
				else
					$last = '';

				$output .= "<a href='{$image->link}' class='instagram-grid-{$grid} {$last}'>";

				$output .= "<img src='";

				// if ( 4 == $grid )
				// 	$output .= $image->images->thumbnail->url;
				// else if ( 3 == $grid )
					$output .= $image->images->low_resolution->url;
				// else if ( 2 >= $grid )
				// 	$output .= $image->images->standard_resolution->url;

				$output .= "' ";

				$output .= " >"; // end of image tag

				$output .= "</a>";
				$i++;
			}

			$output .= "</div>";
		}



		return $output;
	}
}