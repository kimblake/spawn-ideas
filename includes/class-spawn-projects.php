<?php

/**
 * Spawn Portfolio Project Post Type
 */
class Spawn_Projects {
	private static $_instance;

	/**
	 * Setup actions and filters. This is a singleton.
	 *
	 * @since 0.1
	 * @uses add_action, add_filter
	 */
	private function __construct() {
		add_action( 'init', array( $this, 'action_register_cpt' ) );
		add_action( 'init', array( $this, 'action_register_tax' ) );
		add_action( 'save_post', array( $this, 'action_save_post' ) );
		add_filter( 'post_updated_messages', array( $this, 'filter_post_updated_messages' ) );
	}

	/**
	 * Registers post type for providers
	 *
	 * @since 0.1
	 * @return void
	 */
	public function action_register_cpt() {
		$labels = array(
			'name'               => 'Projects',
			'singular_name'      => 'Projects',
			'add_new'            => 'Add New',
			'add_new_item'       => 'Add New Project',
			'edit_item'          => 'Edit Project',
			'new_item'           => 'New Project',
			'all_items'          => 'All Projects',
			'view_item'          => 'View Project',
			'search_items'       => 'Search Projects',
			'not_found'          => 'No projects found',
			'not_found_in_trash' => 'No projects found in trash',
			'parent_item_colon'  => '',
			'menu_name'          => 'Work',
		);

		$args = array(
			'labels'                 => $labels,
			'public'                 => true,
			'publicly_queryable'     => true,
			'show_ui'                => true,
			'show_in_menu'           => true,
			'query_var'              => true,
			'rewrite'                => array( 'slug' => 'work' ),
			'has_archive'            => 'work',
			'hierarchical'           => false,
			'menu_position'          => null,
			'with_front'             => false,
			'supports'               => array( 'title', 'thumbnail', 'editor', 'excerpt' ),
			'register_meta_box_cb'   => array( $this, 'add_meta_boxes' ),
		);

		register_post_type( 'spawn-projects', $args );
		register_taxonomy_for_object_type( 'category', 'spawn-projects' );
    add_post_type_support( 'spawn-projects', 'page-attributes' );
	}

	/**
	 * Filter CPT messages
	 *
	 * @param array $messages
	 * @uses get_permalink, esc_url, wp_post_revision_title, __, add_query_arg
	 * @return array
	 */
	public function filter_post_updated_messages( $messages ) {
		global $post, $post_ID;

		$messages['spawn-projects'] = array(
			0 => '', // Unused. Messages start at index 1.
			1 => sprintf( 'Project post updated. <a href="%s">View post</a>', esc_url( get_permalink( $post_ID ) ) ),
			2 => 'Custom field updated.',
			3 => 'Custom field deleted.',
			4 => 'Project post updated',
			5 => isset( $_GET['revision']) ? sprintf( ' Project post restored to revision from %s', wp_post_revision_title( (int) $_GET['revision'], false ) ) : false,
			6 => sprintf( 'Project post published. <a href="%s">View Post</a>', esc_url( get_permalink( $post_ID) ) ),
			7 => 'Project post saved',
			8 => sprintf( 'Project post submitted. <a target="_blank" href="%s">Preview post</a>', esc_url( add_query_arg( 'preview', 'true', get_permalink( $post_ID ) ) ) ),
			9 => sprintf( 'Project post scheduled for: <strong>%1$s</strong>. <a target="_blank" href="%2$s">Preview post</a>',
				date_i18n( 'M j, Y @ G:i', strtotime( $post->post_date ) ), esc_url( get_permalink( $post_ID ) ) ),
			10 => sprintf( 'Project draft updated. <a target="_blank" href="%s">Preview post</a>', esc_url( add_query_arg( 'preview', 'true', get_permalink( $post_ID ) ) ) ),
		);

		return $messages;
	}

	/**
	 * Registers taxonomies for projects
	 *
	 * @since 0.1
	 * @return void
	 */
	public function action_register_tax() {

		$featured_args = array(
			'hierarchical' => false,
			'show_ui' => false,
			'show_admin_column' => false,
			'query_var' => false,
			'rewrite' => false,
		);

		register_taxonomy( 'spawn-project-featured', array( 'spawn-projects' ), $featured_args );

	}

	/**
	 * Register metaboxes
	 *
	 * @uses add_meta_box
	 * @return void
	 */
	public function add_meta_boxes() {
		add_meta_box( 'spawn_project_details', 'Project Details', array( $this, 'meta_box_options' ), 'spawn-projects', 'normal', 'high' );
		add_meta_box( 'spawn_project_featured', 'Feature on Front Page?', array( $this, 'featured_meta_box_options' ), 'spawn-projects', 'side', 'core' );
	}

	/**
	 * Output Project details meta box
	 *
	 * @param object $post
	 * @return void
	 */
	public function meta_box_options( $post ) {
		wp_nonce_field( 'spawn_project_details_action', 'spawn_project_details' );

		$url = get_post_meta( $post->ID, 'spawn_project_url', true );
		if ( ! empty( $url ) )
			$url = esc_url( $url );

		$problem = get_post_meta( $post->ID, 'spawn_project_problem', true );

		$insight = get_post_meta( $post->ID, 'spawn_project_insight', true );

		$solution = get_post_meta( $post->ID, 'spawn_project_solution', true );

		$results = get_post_meta( $post->ID, 'spawn_project_results', true );

		?>
		<p>
			<label for="url">URL:</label>
			<input class="regular-text" type="url" name="spawn_project_url" id="spawn_project_url" value="<?php echo esc_attr( $url ); ?>">
		</p>

		<p>
			<label for="spawn_project_problem">Problem:</label>
			<input class="regular-text" type="text" name="spawn_project_problem" id="spawn_project_problem" value="<?php echo esc_attr( $problem ); ?>">
		</p>

		<p>
			<label for="spawn_project_insight">Insight:</label>
			<input class="regular-text" type="text" name="spawn_project_insight" id="spawn_project_insight" value="<?php echo esc_attr( $insight ); ?>">
		</p>

		<p>
			<label for="spawn_project_solution">Solution:</label>
			<input class="regular-text" type="text" name="spawn_project_solution" id="spawn_project_solution" value="<?php echo esc_attr( $solution ); ?>">
		</p>

		<p>
			<label for="spawn_project_results">Results:</label>
			<input class="regular-text" type="text" name="spawn_project_results" id="spawn_project_results" value="<?php echo esc_attr( $results ); ?>">
		</p>


	<?php
	}

	/**
	 * Display a meta box for the featured checkbox
	 * @param  object $post
	 * @return void
	 */
	function featured_meta_box_options( $post ) {
		wp_nonce_field( 'additional_options_action', 'additional_options' );
		$featured = has_term( 'project-featured', 'spawn-project-featured', $post );
		echo 'Featured: <input type="checkbox" name="project-featured" value="1" ' . ( ( $featured ) ? 'checked="checked"' : '' ) . '>';
	}


	/**
	 * Save information associated with CPT
	 *
	 * @param int $post_id
	 * @since 0.1
	 * @return void
	 */
	public function action_save_post( $post_id ) {
		if ( ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) || ! current_user_can( 'edit_post', $post_id ) || 'revision' == get_post_type( $post_id ) )
			return;

		if ( ! empty( $_POST['spawn_project_details'] ) && wp_verify_nonce( $_POST['spawn_project_details'], 'spawn_project_details_action' ) ) {

			if ( ! empty( $_POST['spawn_project_url'] ) ) {
				update_post_meta( $post_id, 'spawn_project_url', sanitize_text_field( $_POST['spawn_project_url'] ) );
			} else {
				delete_post_meta( $post_id, 'spawn_project_url' );
			}

			if ( ! empty( $_POST['spawn_project_problem'] ) ) {
				update_post_meta( $post_id, 'spawn_project_problem', sanitize_text_field( $_POST['spawn_project_problem'] ) );
			} else {
				delete_post_meta( $post_id, 'spawn_project_problem' );
			}

			if ( ! empty( $_POST['spawn_project_insight'] ) ) {
				update_post_meta( $post_id, 'spawn_project_insight', sanitize_text_field( $_POST['spawn_project_insight'] ) );
			} else {
				delete_post_meta( $post_id, 'spawn_project_insight' );
			}

			if ( ! empty( $_POST['spawn_project_solution'] ) ) {
				update_post_meta( $post_id, 'spawn_project_solution', sanitize_text_field( $_POST['spawn_project_solution'] ) );
			} else {
				delete_post_meta( $post_id, 'spawn_project_solution' );
			}

			if ( ! empty( $_POST['spawn_project_results'] ) ) {
				update_post_meta( $post_id, 'spawn_project_results', sanitize_text_field( $_POST['spawn_project_results'] ) );
			} else {
				delete_post_meta( $post_id, 'spawn_project_results' );
			}

		}

		if ( ! empty( $_POST['additional_options'] ) && wp_verify_nonce( $_POST['additional_options'], 'additional_options_action' ) ) {
			if ( ! empty( $_POST['project-featured'] ) ) {
				$featured = term_exists( 'project-featured', 'spawn-project-featured' );
				if ( empty( $featured ) ) {
        			$featured = wp_insert_term( 'project-featured', 'spawn-project-featured' );
				}
				wp_set_post_terms( $post_id, array( (int) $featured['term_id'] ), 'spawn-project-featured' );
			} else {
				wp_set_post_terms( $post_id, array(), 'spawn-project-featured' );
			}
		}

	}


	/**
	 * Initialize class and return an instance of it
	 *
	 * @return object
	 */
	public static function init() {
		if ( ! isset( self::$_instance ) ) {

			self::$_instance = new self();
		}

		return self::$_instance;
	}
}

Spawn_Projects::init();

