<?php
/**
 * Contains methods for customizing the theme customization screen.
 *
 * @link http://codex.wordpress.org/Theme_Customization_API
 */
class Spawn_Customize {
	/**
	 * This hooks into 'customize_register' (available as of WP 3.4) and allows
	 * you to add new sections and controls to the Theme Customize screen.
	 *
	 * Note: To enable instant preview, we have to actually write a bit of custom
	 * javascript. See live_preview() for more.
	 *
	 * @see add_action( 'customize_register', $func )
	 * @param WP_Customize_Manager $wp_customize
	 */
	public static function register_settings( $wp_customize ) {
		/**
		 * Register home page settings section
		 */
		$wp_customize->add_section( 'spawn_homepage_banner', array(
			'title'       => __( 'Homepage - Banner', 'spawn' ),
			'priority'    => 110,
			'capability'  => 'edit_theme_options',
			'description' => __( 'Banner section on the homepage of spawnak.com', 'spawn' ),
		) );

		/**
		 * Home Banner Headline
		 */
		$wp_customize->add_setting( 'spawn_headline_text', array(
			'default'    => 'Be Brave',
			'capability' => 'edit_theme_options',
			'type'       => 'option',
		) );

		$wp_customize->add_control( 'spawn_headline_text_control', array(
			'label'    => __( 'Banner Text', 'spawn' ),
			'section'  => 'spawn_homepage_banner',
			'settings' => 'spawn_headline_text',
		) );




		/**
		 * Register home page settings section
		 */
		$wp_customize->add_section( 'spawn_homepage_category', array(
			'title'       => __( 'Homepage - Featured Category', 'spawn' ),
			'priority'    => 120,
			'capability'  => 'edit_theme_options',
			'description' => __( 'Featured category on the homepage of spawnak.com', 'spawn' ),
		) );

		/**
		 * Home Featured Category Title
		 */
		$wp_customize->add_setting( 'spawn_category_title_text', array(
			'default'    => 'Into the Outdoors',
			'capability' => 'edit_theme_options',
			'type'       => 'option',
		) );

		$wp_customize->add_control( 'spawn_category_title_text_control', array(
			'label'    => __( 'Featured Category - Section Title', 'spawn' ),
			'section'  => 'spawn_homepage_category',
			'settings' => 'spawn_category_title_text',
		) );

		/**
		 * Home Banner Category Text
		 */
		$wp_customize->add_setting( 'spawn_featured_category_text', array(
			'default'    => 'We\'ve been on Outside Magazine\'s list of "100 Best Places to Work in America" for three years running – rising to fifth (#5) in 2014. This trifecta of accolades brings us talented applicants from all over. It also means our employee owners stick around, giving our clients have some seriously seasoned ad pros on their business.',
			'capability' => 'edit_theme_options',
			'type'       => 'option',
		) );

		$wp_customize->add_control(
			new Spawn_Customize_Textarea_Control(
				$wp_customize,
				'textarea',
				array(
					'label'    => 'Featured Category - Section Text',
					'section'  => 'spawn_homepage_category',
					'settings' => 'spawn_featured_category_text'
				)
			)
		);

		/**
		 * Home Featured Category Link Text
		 */
		$wp_customize->add_setting( 'spawn_category_link_text', array(
			'default'    => 'More in Outdoor',
			'capability' => 'edit_theme_options',
			'type'       => 'option',
		) );

		$wp_customize->add_control( 'spawn_category_link_text_control', array(
			'label'    => __( 'Featured Category Link Text', 'spawn' ),
			'section'  => 'spawn_homepage_category',
			'settings' => 'spawn_category_link_text',
		) );

		/**
		 * Home Featured Category Link
		 */
		$wp_customize->add_setting( 'spawn_category_link', array(
			'default'    => 'http://spawnak.com/category/outdoor',
			'capability' => 'edit_theme_options',
			'type'       => 'option',
		) );

		$wp_customize->add_control( 'spawn_category_link_control', array(
			'label'    => __( 'Featured Category Link', 'spawn' ),
			'section'  => 'spawn_homepage_category',
			'settings' => 'spawn_category_link',
		) );




		/**
		 * Register home page settings section
		 */
		$wp_customize->add_section( 'spawn_homepage_best_places', array(
			'title'       => __( 'Homepage - Best Places to Work', 'spawn' ),
			'priority'    => 130,
			'capability'  => 'edit_theme_options',
			'description' => __( 'Featured category on the homepage of spawnak.com', 'spawn' ),
		) );

		/**
		 * Home "Best Places to Work" Title
		 */
		$wp_customize->add_setting( 'spawn_best_places_title', array(
			'default'    => 'Outdoor Magazine\'s Best Places to Work 2012-2013',
			'capability' => 'edit_theme_options',
			'type'       => 'option',
		) );

		$wp_customize->add_control( 'spawn_best_places_control', array(
			'label'    => __( 'Best Places to Work - Section Title', 'spawn' ),
			'section'  => 'spawn_homepage_best_places',
			'settings' => 'spawn_best_places_title',
		) );

		/**
		 * Home "Best Places" Text
		 */
		$wp_customize->add_setting( 'spawn_best_places_text', array(
			'default'    => 'Come work with us. All the cool kids are doing it. Plus, we\'ve been one of Outdoor Magazine\'s Best Places to Work for the last two years.',
			'capability' => 'edit_theme_options',
			'type'       => 'option',
		) );

		$wp_customize->add_control(
			new Spawn_Customize_Textarea_Control(
				$wp_customize,
				'spawn_best_places_text',
				array(
					'label'    => 'Best Places to Work - Section Text',
					'section'  => 'spawn_homepage_best_places',
					'settings' => 'spawn_best_places_text'
				)
			)
		);



		/**
		 * Register Awards Section
		 */
		$wp_customize->add_section( 'spawn_about_awards', array(
			'title'       => __( 'About - Awards', 'spawn' ),
			'priority'    => 140,
			'capability'  => 'edit_theme_options',
		) );

		/**
		 * Awards section text
		 */
		$wp_customize->add_setting( 'spawn_awards_text', array(
			'default'    => 'To us, awards are more than flashy pieces of metal.  They\'re proof in the pudding. Confirmation that we know strategy, how to manage business, deliver winning creative and generate results – something we\'re passionate about doing for our Agency, but even more so, for our clients.<ul><li>Outdoor Magazine Best Places to Work 2012, 2013, 2014</li><li>AdWeek Top Shop in Alaska, 2014</li><li>Inc. Magazines Fastest Growing Companies in America, 2014</li><li>AMA Prism Awards, 2014</li><li>AIGA Big Ones, 2014</li></ul>',
			'capability' => 'edit_theme_options',
			'type'       => 'option',
		) );

		$wp_customize->add_control(
			new Spawn_Customize_Textarea_Control(
				$wp_customize,
				'spawn_awards_list',
				array(
					'label'    => 'Awards - Section Text',
					'section'  => 'spawn_about_awards',
					'settings' => 'spawn_awards_text'
				)
			)
		);

		/**
		 * Remove unneeded default sections
		 */
		$wp_customize->remove_section( 'static_front_page' );


		/**
		 * Get settings
		 */
		$wp_customize->get_setting( 'blogname' )->transport                     = 'postMessage';
		$wp_customize->get_setting( 'blogdescription' )->transport              = 'postMessage';
		$wp_customize->get_setting( 'spawn_headline_text' )->transport          = 'postMessage';
		$wp_customize->get_setting( 'spawn_category_title_text' )->transport    = 'postMessage';
		$wp_customize->get_setting( 'spawn_featured_category_text' )->transport = 'postMessage';
		$wp_customize->get_setting( 'spawn_category_link_text' )->transport     = 'postMessage';
		$wp_customize->get_setting( 'spawn_category_link' )->transport          = 'postMessage';
		$wp_customize->get_setting( 'spawn_best_places_title' )->transport      = 'postMessage';
		$wp_customize->get_setting( 'spawn_best_places_text' )->transport       = 'postMessage';
		$wp_customize->get_setting( 'spawn_awards_text' )->transport            = 'postMessage';
	}

	/**
	 * This outputs the javascript needed to automate the live settings preview.
	 * Also keep in mind that this function isn't necessary unless your settings
	 * are using 'transport'=>'postMessage' instead of the default 'transport'
	 * => 'refresh'
	 *
	 * Used by hook: 'customize_preview_init'
	 *
	 * @see add_action( 'customize_preview_init', $func )
	 */
	public static function live_preview() {
		wp_enqueue_script(
			'spawn-theme-customizer',
			get_template_directory_uri() . '/assets/js/admin/theme-customizer.js',
			array( 'jquery', 'customize-preview' ),
			'',
			true
		);
	}
}

// Setup the Theme Customizer settings and controls...
add_action( 'customize_register' , array( 'Spawn_Customize' , 'register_settings' ) );

// Enqueue live preview javascript in Theme Customizer admin screen
add_action( 'customize_preview_init' , array( 'Spawn_Customize' , 'live_preview' ) );


/**
 * Adds textarea support to the theme customizer
 */
if ( isset( $wp_customize ) ) {
	class Spawn_Customize_Textarea_Control extends WP_Customize_Control {
		public $type = 'textarea';

		public function render_content() { ?>
			<label>
				<span class="customize-control-title"><?php echo esc_html( $this->label ); ?></span>
				<textarea rows="5" style="width:100%;" <?php $this->link(); ?>><?php echo esc_textarea( $this->value() ); ?></textarea>
			</label>
		<?php
		}
	}
}