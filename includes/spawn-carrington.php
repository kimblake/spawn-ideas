<?php
/* Site-specific Carrington Build modifications */
require get_template_directory() . '/includes/carrington-build/carrington-build.php';
print_r(get_declared_classes());

/**
 * Four-column row
 */
class cfct_row_abcd extends cfct_build_row {
	public function __construct() {
		$config = array(
			'name' => '4 Column',
			'description' => 'A 4 column row.'
		);

		//  Filters in rows used to be keyed by the single classname
		// that was registered for the class. Maintain backwards
		// compatibility for filters by setting modifier for this row to
		// the old classname property.
		$this->set_filter_mod('cfct-row-a-b-c-d');

		$this->add_classes(array('row-c4-1-2-3-4'));

		$this->push_block(new cfct_block_c4_1);
		$this->push_block(new cfct_block_c4_2);
		$this->push_block(new cfct_block_c4_3);
		$this->push_block(new cfct_block_c4_4);

		parent::__construct($config);
	}
}
cfct_build_register_row('cfct_row_abcd');
