<?php

/**
 * Spawn Portfolio Job Post Type
 */
class Spawn_Jobs {
	private static $_instance;

	/**
	 * Setup actions and filters. This is a singleton.
	 *
	 * @since 0.1
	 * @uses add_action, add_filter
	 */
	private function __construct() {
		add_action( 'init', array( $this, 'action_register_cpt' ) );
		add_action( 'init', array( $this, 'action_register_tax' ) );
		add_action( 'save_post', array( $this, 'action_save_post' ) );
		add_filter( 'post_updated_messages', array( $this, 'filter_post_updated_messages' ) );
	}

	/**
	 * Registers post type for providers
	 *
	 * @since 0.1
	 * @return void
	 */
	public function action_register_cpt() {
		$labels = array(
			'name'               => 'Jobs',
			'singular_name'      => 'Jobs',
			'add_new'            => 'Add New',
			'add_new_item'       => 'Add New Job',
			'edit_item'          => 'Edit Job',
			'new_item'           => 'New Job',
			'all_items'          => 'All Jobs',
			'view_item'          => 'View Job',
			'search_items'       => 'Search Jobs',
			'not_found'          => 'No jobs found',
			'not_found_in_trash' => 'No jobs found in trash',
			'parent_item_colon'  => '',
			'menu_name'          => 'Jobs',
		);

		$args = array(
			'labels'                 => $labels,
			'public'                 => true,
			'publicly_queryable'     => true,
			'show_ui'                => true,
			'show_in_menu'           => true,
			'query_var'              => true,
			'rewrite'                => array( 'slug' => 'careers' ),
			'has_archive'            => false,
			'hierarchical'           => false,
			'menu_position'          => null,
			'with_front'             => false,
			'supports'               => array( 'title', 'editor', 'excerpt' ),
			'register_meta_box_cb'   => array( $this, 'add_meta_boxes' ),
		);

		register_post_type( 'spawn-jobs', $args );
	}

	/**
	 * Filter CPT messages
	 *
	 * @param array $messages
	 * @uses get_permalink, esc_url, wp_post_revision_title, __, add_query_arg
	 * @return array
	 */
	public function filter_post_updated_messages( $messages ) {
		global $post, $post_ID;

		$messages['spawn-jobs'] = array(
			0 => '', // Unused. Messages start at index 1.
			1 => sprintf( 'Job post updated. <a href="%s">View post</a>', esc_url( get_permalink( $post_ID ) ) ),
			2 => 'Custom field updated.',
			3 => 'Custom field deleted.',
			4 => 'Job post updated',
			5 => isset( $_GET['revision']) ? sprintf( ' Job post restored to revision from %s', wp_post_revision_title( (int) $_GET['revision'], false ) ) : false,
			6 => sprintf( 'Job post published. <a href="%s">View Post</a>', esc_url( get_permalink( $post_ID) ) ),
			7 => 'Job post saved',
			8 => sprintf( 'Job post submitted. <a target="_blank" href="%s">Preview post</a>', esc_url( add_query_arg( 'preview', 'true', get_permalink( $post_ID ) ) ) ),
			9 => sprintf( 'Job post scheduled for: <strong>%1$s</strong>. <a target="_blank" href="%2$s">Preview post</a>',
				date_i18n( 'M j, Y @ G:i', strtotime( $post->post_date ) ), esc_url( get_permalink( $post_ID ) ) ),
			10 => sprintf( 'Job draft updated. <a target="_blank" href="%s">Preview post</a>', esc_url( add_query_arg( 'preview', 'true', get_permalink( $post_ID ) ) ) ),
		);

		return $messages;
	}

	/**
	 * Registers taxonomies for projects
	 *
	 * @since 0.1
	 * @return void
	 */
	public function action_register_tax() {
		$active_args = array(
			'hierarchical' => false,
			'show_ui' => false,
			'show_admin_column' => false,
			'query_var' => false,
			'rewrite' => false,
		);

		register_taxonomy( 'spawn-job-active', array( 'spawn-projects' ), $active_args );
	}


	/**
	 * Register metaboxes
	 *
	 * @uses add_meta_box
	 * @return void
	 */
	public function add_meta_boxes() {
		add_meta_box( 'spawn_job_details', 'Job Details', array( $this, 'meta_box_options' ), 'spawn-jobs', 'normal', 'high' );
		add_meta_box( 'spawn_job_active', 'Is this an active search?', array( $this, 'featured_meta_box_options' ), 'spawn-jobs', 'side', 'core' );
	}

	/**
	 * Output job details meta box
	 *
	 * @param object $post
	 * @return void
	 */
	public function meta_box_options( $post ) {
		wp_nonce_field( 'spawn_job_details_action', 'spawn_job_details' );

		$url = get_post_meta( $post->ID, 'spawn_job_apply_url', true );
		if ( ! empty( $url ) )
			$url = esc_url( $url );

		?>

		<p>
			<label for="url">Application URL:</label>
			<input class="regular-text" type="url" name="spawn_job_apply_url" id="spawn_job_apply_url" value="<?php echo esc_attr( $url ); ?>">
		</p>

	<?php
	}

	/**
	 * Display a meta box for the active checkbox
	 * @param  object $post
	 * @return void
	 */
	function featured_meta_box_options( $post ) {
		wp_nonce_field( 'additional_options_action', 'additional_options' );
		$featured = has_term( 'job-active', 'spawn-job-active', $post );
		echo 'Active search: <input type="checkbox" name="job-active" value="1" ' . ( ( $featured ) ? 'checked="checked"' : '' ) . '>';
	}

	/**
	 * Save information associated with CPT
	 *
	 * @param int $post_id
	 * @since 0.1
	 * @return void
	 */
	public function action_save_post( $post_id ) {
		if ( ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) || ! current_user_can( 'edit_post', $post_id ) || 'revision' == get_post_type( $post_id ) )
			return;

		if ( ! empty( $_POST['spawn_job_details'] ) && wp_verify_nonce( $_POST['spawn_job_details'], 'spawn_job_details_action' ) ) {

			if ( ! empty( $_POST['spawn_job_apply_url'] ) ) {
				update_post_meta( $post_id, 'spawn_job_apply_url', sanitize_text_field( $_POST['spawn_job_apply_url'] ) );
			} else {
				delete_post_meta( $post_id, 'spawn_job_apply_url' );
			}

		}

		if ( ! empty( $_POST['additional_options'] ) && wp_verify_nonce( $_POST['additional_options'], 'additional_options_action' ) ) {
			if ( ! empty( $_POST['job-active'] ) ) {
				$featured = term_exists( 'job-active', 'spawn-job-active' );
				if ( empty( $featured ) ) {
        			$featured = wp_insert_term( 'job-active', 'spawn-job-active' );
				}
				wp_set_post_terms( $post_id, array( (int) $featured['term_id'] ), 'spawn-job-active' );
			} else {
				wp_set_post_terms( $post_id, array(), 'spawn-job-active' );
			}
		}

	}


	/**
	 * Initialize class and return an instance of it
	 *
	 * @return object
	 */
	public static function init() {
		if ( ! isset( self::$_instance ) ) {

			self::$_instance = new self();
		}

		return self::$_instance;
	}
}

Spawn_Jobs::init();

