	<div class="awards-list">

	<?php $awards = get_terms( 'awards', array( 'hide_empty' => false) );
	if ( ! empty($awards) ) : ?>
		<ul>

		<?php foreach ( $awards as $award ) : ?>

			<li class="entry-title"><?php echo esc_html( $award->name ); ?></li>

		<?php endforeach; ?>
		</ul>

	<?php endif; ?>
	</div>