<header class="page-header">
				<h1 class="page-title">Services</h1>
			</header><!-- .page-header -->
			
			<div class="top-icons">
    			
    			<div class="icon-table">
    			
    			<ul id="icon-list">
        			<li class="icon-strategy"><a href="/services-strategy/">Strategy</a></li>
        			<li class="icon-creative"><a href="/services-creative/">Creative</a></li>
        			<li class="icon-digital"><a href="/services-digital/">Digital</a></li>
        			<li class="icon-media"><a href="/services-media/">Media</a></li>
        			<li class="icon-production"><a href="/services-production/">Production</a></li>
    			</ul>
    			
    			</div>
    			
			</div>
