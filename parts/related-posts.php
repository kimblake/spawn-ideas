<div class="home-title">
    <h2>Related Posts</h2>
</div>

<div id="post-content">
     <div class="content-area row bxslider">
		<?php
    $orig_post = $post;
    global $post;
    $categories = get_the_category($post->ID);
     
    if ($categories) {
    $category_ids = array();
    foreach($categories as $individual_category) $category_ids[] = $individual_category->term_id;
    $args=array(
  'category__in' => $category_ids,
  'post__not_in' => array($post->ID),
  'posts_per_page'=> 12, // Number of related posts that will be shown.
  'get_posts'=>1
  );
     
    $postrow = new wp_query( $args );
 
    while( $postrow->have_posts() ) {
    $postrow->the_post();
    ?>
     
    <?php get_template_part('content', 'grid'); ?>
     
    <? }
    }
    $post = $orig_post;
    wp_reset_query();
    ?>
</div>
</div>