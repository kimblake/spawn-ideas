<div class="home-title">
    <h2>THE HATCHERY</h2>
</div>

<div id="post-content">
    <div class="content-area row bxslider2">
    
        <?php $args = array(
                'post_type' => 'post',
                'ignore_sticky_posts' => true,
                'posts_per_page'         => 12,
            );

        $postrow = new WP_Query($args);

if ($postrow->have_posts()) : ?>
    
            <?php /* Start the Loop */ ?>
            <?php while ($postrow->have_posts()) :
                $postrow->the_post(); ?>
                <?php get_template_part('content', 'grid'); ?>
            <?php endwhile; ?>
           
        <?php         else : ?>
            <?php get_template_part('content', 'none'); ?>
        <?php endif;
wp_reset_postdata(); ?>
    
    </div>
    <!-- .posts -->
    <div class="content">
        <div class="home-cta">
            <a href="/blog/">See All</a>
        </div>
    </div>
</div>
