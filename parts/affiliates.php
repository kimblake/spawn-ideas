	<div class="affiliates">

	<?php $affiliates = get_terms( 'partners', array( 'hide_empty' => false) );
	if ( ! empty($affiliates) ) :
		$partners_meta = get_option( 'partners_meta' ); ?>

		<ul>
		<?php foreach ( $affiliates as $affiliate ) : ?>
			<li>
			<?php if( ! empty( $partners_meta[$affiliate->term_id]['partner_url'] ) ) { ?>
				<a href="<?php echo esc_url( $partners_meta[$affiliate->term_id]['partner_url'] ); ?>">
					<?php if ( ! empty( $partners_meta[$affiliate->term_id]['tax_image'] ) ) { ?>
						<img src="<?php echo esc_url( $partners_meta[$affiliate->term_id]['tax_image'] ); ?>" width="220" alt="<?php echo esc_attr( $affiliate->name ); ?>"/>
					<?php } else { ?>
						<img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/placeholder.png" />
					<?php } ?>
				</a>
			<?php } else { ?>
				<?php if ( ! empty( $partners_meta[$affiliate->term_id]['tax_image'] ) ) { ?>
					<img src="<?php echo esc_url( $partners_meta[$affiliate->term_id]['tax_image'] ); ?>" width="220" alt="<?php echo esc_attr( $affiliate->name ); ?>"/>
				<?php } else { ?>
					<img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/placeholder.png" />
				<?php } ?>
			<?php } ?>
			</li>
		<?php endforeach; ?>
		</ul>
	<?php endif; ?>
	</div>