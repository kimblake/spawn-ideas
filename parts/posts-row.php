	<div class="content-area posts row">
		<?php $args = array(
				'post_type' => 'post',
				'ignore_sticky_posts' => true,
				'posts_per_page'         => 3,
			);

		$postrow = new WP_Query( $args );

		if ( $postrow->have_posts() ) : ?>
		<a href="<?php echo get_permalink( get_option( 'page_for_posts' ) ) ?>"><div class="narrow block-left posts-header"><h2>Blog</h2><h5>&rarr; THE HATCHERY</h5></div></a>

			<?php /* Start the Loop */ ?>
			<?php while ( $postrow->have_posts() ) : $postrow->the_post(); ?>

				<?php
					get_template_part( 'content', 'grid' );
				?>

			<?php endwhile; ?>

		<?php else : ?>

			<?php get_template_part( 'content', 'none' ); ?>

		<?php endif; wp_reset_postdata(); ?>
	</div><!-- .posts -->
