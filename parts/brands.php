	<div class="brands">

	<?php $args = array(
		'post_type' => 'spawn-brands',
		'posts_per_page' => -1,
		'orderby' => 'menu_order',
		'order' => 'ASC'
		);
	$brands = new WP_Query( $args );

	if ( $brands->have_posts() ) : ?>
			<ul>

		<?php while ( $brands->have_posts() ) : $brands->the_post() ?>
			<li>
			<?php $brand_url = get_post_meta( get_the_ID(), 'spawn_brand_url', true );
			if( ! empty( $brand_url ) ) { ?>
				<a href="<?php echo esc_url( $brand_url ); ?>">
					<?php if ( has_post_thumbnail() ) { ?>
						<?php the_post_thumbnail( 'square', array( 'alt' => get_the_title() ) ); ?>
					<?php } else { ?>
						<img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/placeholder.png" alt="<?php the_title(); ?>"/>
					<?php } ?>
				</a>
			<?php } else { ?>
					<?php if ( has_post_thumbnail() ) { ?>
						<?php the_post_thumbnail( 'square', array( 'alt' => get_the_title() ) ); ?>
					<?php } else { ?>
						<img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/placeholder.png" alt="<?php the_title(); ?>"/>
					<?php } ?>
			<?php } ?>
			</li>

		<?php endwhile; ?>
			</ul>

	<?php endif; ?>
	</div>