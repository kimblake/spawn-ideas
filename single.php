<?php
/**
 * The template for displaying all single posts.
 *
 * @package spawn
 */

get_header(); ?>

		<?php while ( have_posts() ) : the_post(); ?>

			<header class="page-header">
				<h1 class="page-title">
					The Hatchery
				</h1>
			</header><!-- .page-header -->

		<div id="primary" class="content-area">

      <?php spawn_post_nav(); ?>

			<main id="main" class="site-main" role="main">

			<?php get_template_part( 'content', 'single' ); ?>

			<?php
				// If comments are open or we have at least one comment, load up the comment template
				/** if ( comments_open() || '0' != get_comments_number() ) :
					comments_template();
				endif;
			 */?>

		<?php endwhile; // end of the loop. ?>
		

		</main><!-- #main -->
		
	</div><!-- #primary -->
	
 <div class="row">
    <?php get_template_part( 'parts/related-posts' ); ?>
  </div>
	
<?php get_footer(); ?>
