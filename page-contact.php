<?php
/**
 * The template for the contact page.
 *
 * @package spawn
 */

get_header(); ?>

	<div id="primary" class="content-area page-contact">
		<header class="row page-header">
			<h1 class="page-title">
				Contact
			</h1>
		</header>
		<main id="main" class="site-main" role="main">

		<?php get_template_part( 'parts/contact-row' ); ?>

		<!-- Loop through staff to contact -->
		<div class="row contacts">
			<?php
				$contactquery = new WP_Query( array(
					'post_type' => 'spawn-staff',
					'tax_query' => array(
						array(
							'taxonomy' => 'spawn-staff-contact',
							'field'    => 'slug',
							'terms'    => 'staff-contact',
						),
					),
				) );
			?>
			<?php if ( $contactquery->have_posts() ): ?>
				<?php while ( $contactquery->have_posts() ) : $contactquery->the_post(); ?>
					<?php $meta = get_post_meta( get_the_ID() ); ?>
					<article class="contact-block">
						<h3><?php echo($meta['spawn_staff_contactarea'][0]); ?></h3>
						<p class="name"><?php the_title(); ?></p>
						<p class="title"><?php echo($meta['spawn_staff_title'][0]); ?></p>
						<p class="email"><a href="mailto:<?php echo($meta['spawn_staff_email'][0]); ?>">Contact</a></p>
					</article>
				<?php endwhile; // end of the loop. ?>
			<?php endif; ?>
			<?php wp_reset_postdata(); ?>
		</div>
		<!-- Insert newsletter signup form -->
		<div class="row">
			<div class="half block-left">
				<?php if ( $featured_video = get_post_meta( $post->ID, 'spawn_featured_video_url', true ) ) {
					echo wp_oembed_get( esc_url( $featured_video ), array('width'=>600) );
				} else if ( has_post_thumbnail() ) {
					the_post_thumbnail( 'half-width' );
				} ?>
			</div>
			<div class="half block-right form">
				<!-- Begin MailChimp Signup Form -->
				<div id="mc_embed_signup">
				<form action="//spawnak.us4.list-manage.com/subscribe/post?u=b0c5e4dffe9491fd6a5679660&amp;id=306e13a6f4" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
				    <div id="mc_embed_signup_scroll">
					<h3>Email Newsletter Signup</h3>
				<div class="mc-field-group">
					<label for="mce-FNAME" class="screen-reader-text">First Name </label>
					<input type="text" value="" name="FNAME" class="" id="mce-FNAME" placeholder="First Name">
				</div>
				<div class="mc-field-group">
					<label for="mce-LNAME" class="screen-reader-text">Last Name </label>
					<input type="text" value="" name="LNAME" class="" id="mce-LNAME" placeholder="Last Name">
				</div>
				<div class="mc-field-group">
					<label for="mce-MMERGE4" class="screen-reader-text">Company </label>
					<input type="text" value="" name="MMERGE4" class="" id="mce-MMERGE4" placeholder="Company">
				</div>
				<div class="mc-field-group">
					<label for="mce-EMAIL" class="screen-reader-text">Email Address  <span class="asterisk">*</span>
				</label>
					<input type="email" value="" name="EMAIL" class="required email" id="mce-EMAIL" placeholder="Email (required)">
				</div>

					<div id="mce-responses" class="clear">
						<div class="response" id="mce-error-response" style="display:none"></div>
						<div class="response" id="mce-success-response" style="display:none"></div>
					</div>    <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
				    <div style="position: absolute; left: -5000px;"><input type="text" name="b_b0c5e4dffe9491fd6a5679660_0edf201214" tabindex="-1" value=""></div>
				    <div class="clear"><input type="submit" value="Subscribe" name="subscribe" id="mc-embedded-subscribe" class="button"></div>
				    </div>
				</form>
				</div>
				<script type='text/javascript' src='//s3.amazonaws.com/downloads.mailchimp.com/js/mc-validate.js'></script><script type='text/javascript'>(function($) {window.fnames = new Array(); window.ftypes = new Array();fnames[0]='EMAIL';ftypes[0]='email';fnames[1]='FNAME';ftypes[1]='text';fnames[2]='LNAME';ftypes[2]='text';fnames[3]='MMERGE3';ftypes[3]='text';fnames[4]='MMERGE4';ftypes[4]='text';fnames[5]='MMERGE5';ftypes[5]='text';}(jQuery));var $mcj = jQuery.noConflict(true);</script>
				<!--End mc_embed_signup-->
			</div>
		</div>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php get_footer(); ?>
