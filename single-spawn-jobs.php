<?php
/**
 * The template for displaying all single posts.
 *
 * @package spawn
 */

get_header(); ?>

	<header class="page-header">
		<h1 class="page-title">Work for Us</h1>
	</header><!-- .page-header -->


	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

		<?php while ( have_posts() ) : the_post(); ?>

		<h1 class="entry-title"><?php the_title() ?></h1>

		<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

			<div class="entry-content">
				<?php the_content(); ?>
				<?php
					wp_link_pages( array(
						'before' => '<div class="page-links">' . __( 'Pages:', 'spawn' ),
						'after'  => '</div>',
					) );
				?>
			</div><!-- .entry-content -->
			<footer class="entry-footer">
				<?php edit_post_link( __( 'Edit', 'spawn' ), '<span class="edit-link">', '</span>' ); ?>
			</footer><!-- .entry-footer -->
		</article><!-- #post-## -->

			<?php gravity_form('Application', true, false, false, array('position' => get_the_title() ), true); ?>


		<?php endwhile; // end of the loop. ?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php get_footer(); ?>