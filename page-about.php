<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package spawn
 */

$awards_text = get_option( 'spawn_awards_text' );

get_header(); ?>

	<div id="primary" class="content-area page-about">
			<?php while ( have_posts() ) : the_post(); ?>

		<div class="page-header">
			<h1><?php the_title(); ?></h1>
		</div>

		<main id="main" class="site-main" role="main">

			<div class="entry-content">
				<div class="row about">
					<div class="half block-left about-text">
							<?php the_content(); ?>
					</div>
					<div class="half block-right">
						<?php if ( $featured_video = get_post_meta( $post->ID, 'spawn_featured_video_url', true ) ) {
							echo wp_oembed_get( esc_url( $featured_video ), array('width'=>600) );
						} else if ( has_post_thumbnail() ) {
							the_post_thumbnail( 'half-width' );
						} ?>
					</div>
				</div>

			</div><!-- .entry-content -->
			<?php endwhile; // end of the loop. ?>

			<h2 class="row divider leadership">Leadership</h2>
			<div class="row leaders">

			<?php // leadership bios
			$args = array(
				'post_type' => 'spawn-staff',
				'orderby'	=> 'menu_order',
				'order'		=> 'ASC',
				'tax_query' => array(
					array(
						'taxonomy' => 'spawn-staff-leadership',
						'field'    => 'slug',
						'terms'    => 'staff-leadership',
					),
				),
			);

			$leads = new WP_Query( $args );
			if ( $leads->have_posts() ) : while ( $leads->have_posts() ) : $leads->the_post(); ?>

			<div class="leaderbio">
				<div class="bio-small">
				<?php the_post_thumbnail( 'headshot' ); ?>
				<h3><?php the_title() ?></h3>
				<h4><?php echo get_post_meta( get_the_ID(), 'spawn_staff_title', true ) ?></h4>
				</div>
				<div class="bio-full row">
					<div class="narrow block-left">
						<?php if ( class_exists( 'MultiPostThumbnails' ) ) {
							MultiPostThumbnails::the_post_thumbnail( get_post_type(), 'secondary-image', get_the_ID(), 'square' );
						} ?>
						<?php the_post_thumbnail( 'headshot' ); ?>
					</div>
					<div class="wide block-right">
						<span class="close"></span>
						<h3><?php the_title() ?></h3>
						<h4><?php echo get_post_meta( get_the_ID(), 'spawn_staff_title', true ) ?></h4>
						<?php the_content(); ?>
					</div>
				</div>
			</div>
			<?php endwhile; endif; ?>
			</div>

			<div class="row howwethink"><h3>Basecamp: Alaska</h3>
			<p>Our HQ is in Alaska, but we’re also in Seattle and Denver. There’s something about the air up here, and size and scope of our corner of the world that makes us diverse, resourceful and driven – in work and in play. With video conferencing and all the other digital gadgets available, working with clients in person or remotely from anywhere is efficient, effective, and simple. We can do a face-to-face in your market or you can fly north to do a little work (wink wink). Don’t forget your fishing pole, hiking boots and camera.</p>
			</div>
			
			<div class="row bluebg">
					<div class="half block-left">
  					
  					<div class="about2-content">
  					
  					See how life “Up Here” inspires Spawn employees in work and play.
  					</div>		
				</div>
					<div class="half block-right">
  					<?php putRevSlider("employee-blogs-sm") ?>
					</div>
				</div>

			</div>

			<div class="row awards">
				<div class="block-left narrow"><h2>Awards</h2></div>
				<div class="block-right wide">
					<div id="js-awards-text" class="awards-text">
					<?php dynamic_sidebar( 'award-logos' ); ?>
					</div>
				</div>
			</div>

			<div class="row brand-grid">
				<h2>Brands We've Worked With</h2>
				<?php get_template_part( 'parts/brands' ); ?>

				<h2>Affiliates</h2>
				<?php get_template_part( 'parts/affiliates' ); ?>

			</div>
		</main><!-- #main -->
	</div><!-- #primary -->
		<?php get_template_part( 'parts/contact-row' ); ?>

<?php get_footer(); ?>
