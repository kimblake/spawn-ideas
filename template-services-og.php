<?php
/**
 * Template Name: Services
 */

$terms = get_terms( 'services', array( 'hide_empty' => false, 'orderby' => 'slug' ) );
$services_meta = get_option( 'services_meta' );

get_header(); ?>

	<section id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

		<?php if ( ! empty($terms) ) : ?>

			<header class="page-header">
				<h1 class="page-title">Services</h1>
			</header><!-- .page-header -->

			<?php /* Start the Loop */ ?>
			<?php foreach ( $terms as $term ) :
				$the_term = $term->name; ?>
				<article class="service">
					<header class="entry-header">
						<div class="service-logo <?php echo esc_html( strtolower( $the_term ) ); ?>"></div>
						<h3 class="entry-title"><?php echo esc_html( $the_term ); ?></h3>
					</header><!-- header -->

					<section class="service-details">
						<div class="service-description">
						<?php echo esc_html( $term->description ); ?>
						</div>
						<div class="service-footer">
						<?php
						$servicequery = new WP_Query( array(
							'post_type'      => 'spawn-projects',
							'post_status'    => 'publish',
							'posts_per_page' => 3,
						  'orderby'		=> 'menu_order',
						  'order'			=> 'ASC',
							'tax_query'      => ( array( array( 'taxonomy' => 'services', 'fields' => 'ID', 'terms' => ( array( $term->term_id ) ) ) ) )
						) );
						if ( $servicequery->have_posts() ) : ?>
						<h5><a href="<?php echo esc_url( home_url() ); ?>/work/?work=<?php esc_html_e( $term->term_id, 'spawn' ); ?>" class="services-link">Our work in <?php esc_html_e( $term->name, 'spawn' ); ?></a></h5>
							<div>
							<?php while ( $servicequery->have_posts() ) : $servicequery->the_post(); ?>
								<a href="<?php the_permalink(); ?>"><?php the_post_thumbnail( 'square-small' ) ?></a>
							<?php endwhile; ?>
							</div>
						<?php endif; wp_reset_postdata(); ?>
						&rarr; <a href="<?php echo esc_url( $services_meta[$term->term_id]['service_cta_url'] ) ?>" class="sign-up"><?php echo esc_html( $services_meta[$term->term_id]['service_cta_text'] ) ?></a>
						</div>
					</section><!-- details -->
				</article>

			<?php endforeach; ?>

		<?php else : ?>

			<?php get_template_part( 'content', 'none' ); ?>

		<?php endif; ?>

		</main><!-- #main -->
	</section><!-- #primary -->

<?php get_footer(); ?>
