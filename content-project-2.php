<?php
/**
 * @package spawn
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<div class="entry-content" id="entry">

		<?php the_content(); ?>

		<?php
			wp_link_pages( array(
				'before' => '<div class="page-links">' . __( 'Pages:', 'spawn' ),
				'after'  => '</div>',
			) );
		?>
	</div><!-- .entry-content -->
	<footer class="entry-footer">
		<?php if ( ! empty($details['spawn_project_url'][0]) ) { ?>
			<p><a href="<?php echo esc_url( $details['spawn_project_url'][0] )?>">Visit the site</a></p>
		<?php } ?>
		<?php $categories = get_the_term_list( $post->ID, 'category', 'Categories: ', ', ' );
			if ( ! empty( $categories ) ) {
				echo $categories . '</br>';
			}
		?>
		<?php $services = get_the_term_list( $post->ID, 'services', 'Services: ', ', ' );
			if ( ! empty( $services ) ) {
				echo $services . '</br>';
			}
		?>
		<?php $keywords = get_the_term_list( $post->ID, 'keywords', 'Keywords: ', ', ' );
			if ( ! empty( $keywords ) ) {
				echo $keywords . '</br>';
			}
		?>
		<?php $awards = strip_tags( get_the_term_list( $post->ID, 'awards', 'Awards: ', ', ' ) );
			if ( ! empty( $awards ) ) {
				echo $awards . '</br>';
			}
		?>


		<?php edit_post_link( __( 'Edit', 'spawn' ), '<span class="edit-link">', '</span>' ); ?>
	</footer><!-- .entry-footer -->
</article><!-- #post-## -->
