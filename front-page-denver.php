<?php
/**
 * The front page template file for Denver
 *
 * Template Name: Denver
 *
 * @package Spawn Ideas
 * @since 0.1.0
 */

$category_title     = get_option( 'spawn_category_title_text' );
$category_text      = get_option( 'spawn_featured_category_text' );
$category_link_text = get_option( 'spawn_category_link_text' );
$category_link      = get_option( 'spawn_category_link' );
$best_places_title  = get_option( 'spawn_best_places_title' );
$best_places_text   = get_option( 'spawn_best_places_text' );

get_header(); ?>

    <div class="row">
        <?php get_template_part( 'parts/services-menu-home' ); ?>
    </div>
    
    <div class="row cta-home-contact">
    <?php get_template_part( 'parts/services-cta' ); ?>
    </div>

	<div id="primary">
		<div class="row about">
			<div class="half block-left about-text">
				<?php /* Start the Loop */ ?>
				<?php while ( have_posts() ) : the_post(); ?>

					<?php the_content(); ?>

				<?php endwhile; ?>
				&rarr; <a href="/about" class="about-link">About</a>
			</div>
			<div class="half block-right">
				<?php if ( has_post_thumbnail() ) {
					the_post_thumbnail( 'half-width' );
				} else { ?>
					<img src="http://lorempixel.com/g/600/320/">
				<?php } ?>
			</div>
		</div>

		<div class="row work">
			<div id="work-slider">
				<?php $args = array(
					'post_type' => 'spawn-projects',
					'posts_per_page' => 3,
					'orderby' => 'rand',
					'tax_query' => array(
						array(
							'taxonomy' => 'spawn-project-featured',
							'field'    => 'slug',
							'terms'    => 'project-featured',
						),
					),
				);

				$projectrand = new WP_Query( $args );

				if ( $projectrand->have_posts() ) : ?>

					<?php /* Start the Loop */ ?>
					<?php while ( $projectrand->have_posts() ) : $projectrand->the_post(); ?>

						<article class="featured-work">
							<div class="half block-left featured-image">
								<?php
									if ( class_exists( 'MultiPostThumbnails' ) ) {
										MultiPostThumbnails::the_post_thumbnail( get_post_type(), 'secondary-image', get_the_ID(), 'half-width' );
									} else {
										the_post_thumbnail( 'half-width' );
									}
								?>
							</div>

							<div class="half block-right featured-text">
								<div class="half block-left">
									<h2><?php $cat = get_the_category_list(' '); echo strip_tags( $cat ); ?></h2>

									<div class="article-dots"><span class="dot"></span><span class="dot"></span><span class="dot"></span></div>
								</div>
								<div class="half block-right">
									<h3><a href="<?php the_permalink() ?>"><?php the_title() ?></a></h3>
									<span class="client"><?php echo strip_tags( get_the_term_list( get_the_ID(), 'brands', '', ' ', '' ) ); ?></span>
									<p><?php the_excerpt(); ?>
									</p>
									<!--<h5>&rarr; <span class="more">More in <?php echo $cat; ?></span></h5> -->
								</div>
							</div>
						</article>

					<?php endwhile; ?>
				<?php endif; ?>
			</div>
		</div>

		

		<div class="row home-cat si-feature">
			<h3 id="js-category-title"><?php esc_html_e( $category_title ); ?></h3>
			<p id="js-category-text"><?php esc_html_e( $category_text ); ?></p>
			<p>&rarr; <a href="<?php echo esc_url( $category_link ); ?>" id="js-category-link"><?php esc_html_e( $category_link_text ); ?></a></p>
			</div>

		</div>

		<?php get_template_part( 'parts/posts-row' ); ?>

		<div class="row home-gallery">
		<div class="half block-left large"><img src=<?php echo esc_url( get_template_directory_uri() ) . '/images/spawn-ak-team.jpg'?>></div>
			<div class="half block-left"><div class="half block-left"><img src=<?php echo esc_url( get_template_directory_uri() ) . '/images/spawn-ak-path.jpg'?>><img src=<?php echo esc_url( get_template_directory_uri() ) . '/images/spawn-ak-train.jpg'?>></div><div class="half block-left"><img src=<?php echo esc_url( get_template_directory_uri() ) . '/images/spawn-ak-hike.jpg'?>><img src=<?php echo esc_url( get_template_directory_uri() ) . '/images/spawn-ak-fish.jpg'?>></div></div>
		</div>

		<div class="row careers">
			<div class="narrow block-left career-header">
    			<h2>Best<br>Places To Work</h2></div>
			<div class="wide block-right">
			<h4 id="js-best-places-title"><?php esc_html_e( $best_places_title ); ?></h4>
			<div id="js-best-places-text"><p><?php esc_html_e( $best_places_text ); ?></p></div>
			<a href="/careers"><h5>&rarr; <?php esc_html_e( 'CAREERS', 'spawn' ); ?></a></h5></div>
		</div>

	</div><!-- #primary -->
		<?php get_template_part( 'parts/contact-row' ); ?>

<?php get_footer(); ?>