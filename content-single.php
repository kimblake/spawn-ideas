<?php
/**
 * @package spawn
 */
?>

<article d="post-<?php the_ID(); ?>" <?php post_class(); ?>>

	<div class="entry-title">
		<h1><?php the_title(); ?></h1>
	</div>

	<div class="entry-content">
    <?php $hide = get_post_meta(get_the_ID(), 'hide_featured_image', true);
          if(empty($hide) || $hide == '0') { the_post_thumbnail('square'); } ?>
		<?php the_content(); ?>
		<?php
			wp_link_pages( array(
				'before' => '<div class="page-links">' . __( 'Pages:', 'spawn' ),
				'after'  => '</div>',
			) );
		?>
	</div><!-- .entry-content -->

	<footer class="entry-footer">
		<?php
			/* translators: used between list items, there is a space after the comma */
			$category_list = get_the_category_list( __( ', ', 'spawn' ) );

			/* translators: used between list items, there is a space after the comma */
			$tag_list = get_the_tag_list( '', __( ', ', 'spawn' ) );

			if ( ! spawn_categorized_blog() ) {
				// This blog only has 1 category so we just need to worry about tags in the meta text
				if ( '' != $tag_list ) {
					$meta_text = __( 'Tags: %2$s.', 'spawn' );
				}

			} else {
				// But this blog has loads of categories so we should probably display them here
				if ( '' != $tag_list ) {
					$meta_text = __( 'Categories: %1$s. Tags: %2$s.', 'spawn' );
				} else {
					$meta_text = __( 'Categories: %1$s.', 'spawn' );
				}

			} // end check for categories on this blog

			printf(
				$meta_text,
				$category_list,
				$tag_list
			);
		?>

		<?php edit_post_link( __( 'Edit', 'spawn' ), '<span class="edit-link">', '</span>' ); ?>
	</footer><!-- .entry-footer -->
</article><!-- #post-## -->
