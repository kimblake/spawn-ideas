<?php
/**
 * @package spawn
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<div class="entry-content" id="entry">
		<div class="project-sidebar" id="project-sidebar">
		<?php $details = get_post_meta( get_the_ID() );
			  $client = strip_tags( get_the_term_list( get_the_ID(), 'brands', '', ' ', '') ); ?>
		<?php if ( ! empty ( $client ) ) { ?>
			<h3>Client</h3>
			<p><?php echo esc_html( $client ); ?></p>
		<?php } ?>
		<?php if ( ! empty( $details['spawn_project_problem'][0] ) ) { ?>
			<h3>Challenge</h3>
			<p><?php echo esc_html( $details['spawn_project_problem'][0] ) ?></p>
		<?php } ?>
		<?php if ( ! empty( $details['spawn_project_insight'][0] ) ) { ?>
			<h3>Insight</h3>
			<p><?php echo esc_html( $details['spawn_project_insight'][0] ) ?></p>
		<?php } ?>
		<?php if ( ! empty( $details['spawn_project_solution'][0] ) ) { ?>
			<h3>Solution</h3>
			<p><?php echo esc_html( $details['spawn_project_solution'][0] ) ?></p>
		<?php } ?>
		<?php if ( ! empty( $details['spawn_project_results'][0] ) ) { ?>
			<h3>Results</h3>
			<p><?php echo esc_html( $details['spawn_project_results'][0] ) ?></p>
		<?php } ?>

		</div>
		<?php the_content(); ?>

		<?php
			wp_link_pages( array(
				'before' => '<div class="page-links">' . __( 'Pages:', 'spawn' ),
				'after'  => '</div>',
			) );
		?>
	</div><!-- .entry-content -->
	<footer class="entry-footer">
		<?php if ( ! empty($details['spawn_project_url'][0]) ) { ?>
			<p><a href="<?php echo esc_url( $details['spawn_project_url'][0] )?>">Visit the site</a></p>
		<?php } ?>
		<?php $categories = get_the_term_list( $post->ID, 'category', 'Categories: ', ', ' );
			if ( ! empty( $categories ) ) {
				echo $categories . '</br>';
			}
		?>
		<?php $services = get_the_term_list( $post->ID, 'services', 'Services: ', ', ' );
			if ( ! empty( $services ) ) {
				echo $services . '</br>';
			}
		?>
		<?php $keywords = get_the_term_list( $post->ID, 'keywords', 'Keywords: ', ', ' );
			if ( ! empty( $keywords ) ) {
				echo $keywords . '</br>';
			}
		?>
		<?php $awards = strip_tags( get_the_term_list( $post->ID, 'awards', 'Awards: ', ', ' ) );
			if ( ! empty( $awards ) ) {
				echo $awards . '</br>';
			}
		?>


		<?php edit_post_link( __( 'Edit', 'spawn' ), '<span class="edit-link">', '</span>' ); ?>
	</footer><!-- .entry-footer -->
</article><!-- #post-## -->
