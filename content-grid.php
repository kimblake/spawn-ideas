<?php
/**
 * @package spawn
 */
?>
<div class="narrow block-left content-grid">
	<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
		<a href="<?php echo esc_url( get_permalink() ); ?>" rel="bookmark">
		    <div class="thumbnail slide">
		        <?php if ( has_post_thumbnail() ) {
				        the_post_thumbnail( 'square' );
			      } ?>
        </div>
		<header class="entry-header">
		<h2 class="entry-title"><?php the_title() ?></h2>

			<?php $clients = get_the_terms( get_the_ID(), 'brands' );
			if ( ! empty( $clients ) ) { ?>
				<h3 class="entry-client">
			<?php foreach ( $clients as $client ) {
				echo esc_html( $client->name );
			} ?> </h3><?php } ?>
		</header><!-- .entry-header -->
		</a>
	</article><!-- #post-## -->
</div>
