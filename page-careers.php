<?php
/**
 * The template for displaying archive pages.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package spawn
 */

get_header(); ?>

	<section id="primary" class="content-area page-careers">
		<main id="main" class="site-main" role="main">

		<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

			<header class="page-header">
				<h1 class="page-title">
					Careers
				</h1>
			</header><!-- .page-header -->

			<div class="row jobs">
				<div class="half block-left jobs-text">
					<?php the_content(); ?>
				</div>
				<div class="half block-right">
					<?php if ( $featured_video = get_post_meta( $post->ID, 'spawn_featured_video_url', true ) ) {
						echo wp_oembed_get( esc_url( $featured_video ), array('width'=>600) );
					} else if ( has_post_thumbnail() ) {
						the_post_thumbnail( 'half-width' );
					} ?>
				</div>
			</div>
		<?php wp_reset_postdata(); ?>
		<?php endwhile; endif; ?>

		<?php
		/**
		 * INSTAGRAM WIDGET
		 */
		$mp_instagram_parameter = '@spawnak';
		if ( ! empty( $mp_instagram_parameter ) ) {
			$mp_instagram = new MP_Instagram_Widget;
			$mp_instagram_output = $mp_instagram->get_instagram_widget( $mp_instagram_parameter );


			if ( ! empty( $mp_instagram_output ) )
				echo $mp_instagram_output;
		}
		?>

		<div class="row divider open-jobs">
			<h2>Current Positions</h2>
		</div>
		<section class="row jobs-list">
			<?php /* Start the Loop */ ?>
			<?php
			$jobargs = array(
				'post_type' => 'spawn-jobs',
				);
			$jobsquery = new WP_Query( $jobargs );
			if ( $jobsquery->have_posts() ) :
			while ( $jobsquery->have_posts() ) : $jobsquery->the_post(); ?>

				<?php
					get_template_part( 'content', 'job' );
				?>

			<?php endwhile; ?>

			<?php spawn_paging_nav(); ?>

		<?php endif; ?>
		</section>

		</main><!-- #main -->
	</section><!-- #primary -->

<?php get_footer(); ?>
