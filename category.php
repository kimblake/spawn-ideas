<?php
/**
 * The template for displaying archive pages.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package spawn
 */
get_header();
$category_meta = get_option( 'category_meta' ); ?>

	<section id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

			<header class="row page-header">
				<h1 class="page-title">
					<?php single_term_title(); ?>
				</h1>
			</header><!-- .page-header -->

			<div class="row category-header">
				<div class="block-left half featured-image">
					<?php if ( isset ( $category_meta[$cat]['tax_image'] ) ) { ?>
					<img src="<?php echo esc_url( $category_meta[$cat]['tax_image'] ); ?>" width="600" alt="<?php single_cat_title( '', true ); ?>"/>
					<?php } ?>
				</div>

				<?php
					// Show an optional term description.
					$term_description = term_description();
					$term = get_queried_object();

					if ( ! empty( $term_description ) ) :
						printf( '<div class="taxonomy-description half block-right">%s</div>', $term_description );
					endif;
				?>
			</div>

			<div class="row divider category-title"><h3>We Know <?php single_term_title(); ?></h3></div>
			
	</div>
	
	
	<section class="row brand-list">
				<?php
					$args = array(
						'post_type'      => 'spawn-brands',
						'posts_per_page' => -1,
						'post_status'    => 'publish',
						'orderby'		=> 'menu_order',
						'order'			=> 'ASC',
						'tax_query' => array( array(
							'taxonomy' => 'category',
							'field'    => 'slug',
							'terms'    => get_queried_object()->slug,
						) )
					);

				$brands = new WP_Query( $args );

				if ( $brands->have_posts() ) : ?>

					<h4>Brands</h4>
						<div class="thumbs">
					<?php while ( $brands->have_posts() ) : $brands->the_post();

						if ( has_post_thumbnail() ) {
							the_post_thumbnail( 'square-small' );
						} else {
							the_title();
						}

					endwhile; ?>
						</div>
				<?php endif; wp_reset_postdata(); ?>
			</section>


  <?php get_template_part( 'parts/services-cta' ); ?>   
                                
  </main><!-- #main -->
            
</section><!-- #primary -->

<?php get_footer(); ?>
