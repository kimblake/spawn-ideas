<?php
/**
 * The template for displaying archive pages.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package spawn
 */

get_header(); ?>

	<section id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

		<?php if ( have_posts() ) : ?>

			<header class="row page-header">
				<h1 class="page-title">
					<?php
						single_term_title();
					?>
				</h1>
				<?php
					// Show an optional term description.
					$term_description = term_description();
					$term = get_queried_object();
					$meta = get_option( 'industries_meta' );
					$tax_image_id = $meta[$term->term_id]['tax_image_id'];

					if ( $meta && isset( $tax_image_id ) ): ?>
					<div class="half block-left">
						<?php echo wp_get_attachment_image( $tax_image_id, 'half-width' ); ?>
					</div>
					<?php endif; ?>

					<?php if ( ! empty( $term_description ) ) :
						printf( '<div class="taxonomy-description half block-right">%s</div>', $term_description );
					endif;
				?>
			</header><!-- .page-header -->
			<div class="row divider"><h3>Highlights</h3></div>
			<section class="row post-list">

				<?php /* Start the Loop */ ?>
				<?php while ( have_posts() ) : the_post(); ?>

					<?php
						get_template_part( 'content', 'list' );
					?>

				<?php endwhile; ?>

				<?php spawn_paging_nav(); ?>

			</section>

		<?php else : ?>

			<?php get_template_part( 'content', 'none' ); ?>

		<?php endif; ?>
		</main><!-- #main -->
	</section><!-- #primary -->

	<?php get_template_part( 'parts/posts-row' ); ?>


<?php get_footer(); ?>
