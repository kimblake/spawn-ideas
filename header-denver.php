<?php
if ( substr_count( $_SERVER['HTTP_ACCEPT_ENCODING'], 'gzip' ) ) {
    ob_start( "ob_gzhandler" );
}
else {
    ob_start();
}
?>

<?php
/**
 * The template for displaying the header.
 *
 * @package Spawn Ideas
 * @since 0.1.0
 */
$headline_text = get_option( 'spawn_headline_text' ); ?>

<!DOCTYPE html>
<!--[if  lte IE 8]><html class="lt-ie10 lt-ie9"> <![endif]-->
<!--[if IE 9]><html class="lt-ie10"> <![endif]-->
<!--[if gt IE 9]><!--> <html <?php language_attributes(); ?>> <!--<![endif]-->
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title><?php wp_title( '|', true, 'right' ); ?></title>
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
<link rel="shortcut icon" href="<?php echo get_stylesheet_directory_uri(); ?>/favicon.ico" />

<!--[if lt IE 9]>
	<script src="<?php echo esc_url( get_template_directory_uri() ); ?>/assets/js/vendor/html5shiv.min.js"></script>
	<script src="<?php echo esc_url( get_template_directory_uri() ); ?>/assets/js/vendor/respond.min.js"></script>
<![endif]-->

<?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
    
<!-- Google Tag Manager -->
<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-MP74LT"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-MP74LT');</script>
<!-- End Google Tag Manager -->
 
<div id="page" class="hfeed site">
	<header id="masthead" class="site-header" role="banner">			
			
    <nav id="site-navigation" class="main-navigation" role="navigation">
    			
       <!-- <button class="menu-toggle"><img src="<?php echo get_template_directory_uri() . '/images/menu.svg' ?>"></button>-->

            <div class="site-branding">
				<h1 class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home">
    				<span class="screen-reader-text">
    				    <?php bloginfo( 'name' ); ?>
    				</span>
    				
    				<img src="<?php echo get_template_directory_uri() . '/images/spawnlogo.svg' ?>"></a></h1>
			</div>
					
        <?php wp_nav_menu( array( 'theme_location' => 'primary', 'menu_class' => 'nav-menu' ) ); ?>					

      <?php if(is_front_page()) { ?>
          <div class="hero-title">
              <span class="hero-title-carrot"></span>
              <span class="title"></span>
              <span class="subtitle"></span>
          </div>
      <?php } ?>
		</nav><!-- #site-navigation -->

		<?php if ( is_front_page() || is_page('denver') ) : ?>
		
		
			<div class="page-header">
        <?php echo do_shortcode('[rev_slider home]'); ?>
				<h1 id="js-banner-text"><?php echo wp_kses( $headline_text, array( 'br' => array() ) ); ?></h1>
			<!-- <?php wp_nav_menu( array( 'theme_location' => 'hero', 'menu_class' => 'hero-menu' ) ); ?> -->
			</div>
		<?php endif; ?>
		
        <?php if (is_page('thank-you') || is_page('new-business-inquiries')) { ?>
            <script type="text/javascript">
                var $mcGoal = {'settings':{'uuid':'b0c5e4dffe9491fd6a5679660','dc':'us4'}};
                (function() {
                     var sp = document.createElement('script'); sp.type = 'text/javascript'; sp.async = true; sp.defer = true;
                    sp.src = ('https:' == document.location.protocol ? 'https://s3.amazonaws.com/downloads.mailchimp.com' : 'http://downloads.mailchimp.com') + '/js/goal.min.js';
                    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(sp, s);
                })(); 
            </script>
        <?php } ?>

	</header><!-- #masthead -->

	<div id="content" class="site-content">
