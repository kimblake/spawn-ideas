<?php
/**
 * The template for displaying all single posts.
 *
 * @package spawn
 */

get_header(); ?>

	<div id="primary" class="content-area project-content">
		<main id="main" class="site-main" role="main">

		<?php while ( have_posts() ) : the_post(); ?>

			<header class="page-header">
				<h1 class="page-title">
					<?php the_title(); ?>
				</h1>
				<?php spawn_post_nav() ?>

			</header><!-- .page-header -->


			<?php get_template_part( 'content', 'project' ); ?>

		<?php endwhile; // end of the loop. ?>

<!-- 		<h2 class="row divider related">Related Work</h2> -->

		<section class="row work-list">
		<?php
		$categories = wp_get_post_categories( $post->ID );
		$args = array(
				'post_type'      => 'spawn-projects',
				'posts_per_page' => 3,
				'post__not_in'   => array( $post->ID ),
				'tax_query' => array(array(
					'taxonomy' => 'category',
					'field'    => 'id',
					'terms'    => $categories
				))
			);

		$works = new WP_Query( $args );

		if ( $works->have_posts() ) : ?>

			<?php /* Start the Loop */ ?>
			<?php while ( $works->have_posts() ) : $works->the_post(); ?>

				<?php
// 					get_template_part( 'content', 'grid' );
				?>

			<?php endwhile; ?>
		<?php endif; wp_reset_postdata(); ?>

		</section>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php get_footer(); ?>