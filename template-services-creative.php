<?php
/**
 * Template Name: Services Creative
 */
        $terms = get_terms( 'services', 
            array( 
                'hide_empty'    => false, 
                'orderby'       => 'slug',
                'order'         => 'ASC', 
                'number'        => '9',
                'slug'          => array('concept-developement', 'copywriting', 'graphic-design', 'merchandise', 'out-of-home-advertising', 'print', 'radio', 'video'),
               
            ) 
        );
        
        $services_meta = get_option( 'services_meta' );

get_header(); ?>

	<section id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

		<?php if ( ! empty($terms) ) : ?>

			<?php get_template_part( 'parts/services-menu' ); ?>
			
            <div class="row category-header">
                
                <div class="block-left half featured-image">
					<?php if ( $featured_video = get_post_meta( $post->ID, 'spawn_featured_video_url', true ) ) {
						echo wp_oembed_get( esc_url( $featured_video ), array('width'=>600) );
					} else if ( has_post_thumbnail() ) {
						the_post_thumbnail( 'half-width' );
					} ?>
				</div>
                
				<div class="services-sub-text half block-right">
					<?php the_content(); ?>
				</div>
				
			</div>
			
			<div class="row divider services-title">
    				<h2>
        				<?php echo get_the_title( $ID ); ?> 
                    </h2>
                </div>			

		
			<?php /* Start the Loop */ ?>		
           

    		<?php foreach ( $terms as $term ) :
				$the_term = $term->name; ?> 
            
            <?php
                /**
				<article class="service">
					<header class="entry-header"> 
                       
                       <?php if ( ! empty( $services_meta[$term->term_id]['service_icon'] ) ) { ?>
						<img src="<?php echo esc_url( $services_meta[$term->term_id]['service_icon'] ); ?>" width="" alt="<?php echo esc_attr( $services->name ); ?>"/>
					<?php } else { ?>
						<img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/placeholder.png" />
					<?php } ?>
                       
						<h3 class="service-title"><?php echo esc_html( $the_term ); ?></h3>
					</header><!-- header -->					

					<section class="service-details">
						<div class="service-description">
						<?php echo( $term->description ); ?>
						
						</div>
						
					</section><!-- details -->

				</article>*/ ?>
			<!-- end service wrap -->
			
        <?php endforeach; ?>

		<?php else : ?>

			<?php get_template_part( 'content', 'none' ); ?>

		<?php endif; ?>
		
		<section class="content-area posts row work-serv-proj">    		
			
				<div class="narrow block-left posts-header"><h2 class ="work-in"><small>Work In</small> Creative</h2></div>
								        			
				<?php
					$args = array(
						'post_type'      => 'spawn-projects',
						'posts_per_page' => 3,
						'post_status'    => 'publish',
						'orderby'		=> 'menu_order',
						'order'			=> 'ASC',						
						'tax_query' => array( array(
							'taxonomy' => 'category',							
							'field'    => 'slug',
							'terms'    => 'Energy',
						) )
					);

				$works = new WP_Query( $args );

				if ( $works->have_posts() ) : ?>

					<?php /* Start the Loop */ ?>
					<?php while ( $works->have_posts() ) : $works->the_post(); ?>
					
						<?php get_template_part( 'content', 'grid' ); ?>

					<?php endwhile; ?>
				<?php endif; wp_reset_postdata(); ?>			
			
		</section>
			
		
		<?php get_template_part( 'parts/services-cta' ); ?>

		</main><!-- #main -->
	</section><!-- #primary -->

<?php get_footer(); ?>
