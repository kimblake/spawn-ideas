<?php
/**
 * Template Name: Services Detail
 */
get_header(); ?>

	<section id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

			 <?php get_template_part( 'parts/services-menu' ); ?>

			<div class="row category-header">
  			
  		  <div class="block-left half featured-image">
				  <?php if ( $featured_video = get_post_meta( $post->ID, 'spawn_featured_video_url', true ) ) {
						echo wp_oembed_get( esc_url( $featured_video ), array('width'=>600) );
					} else if ( has_post_thumbnail() ) {
						the_post_thumbnail();
					} ?>
				</div>        
                
        <div class="services-sub-text half block-right">
          <?php the_field('services_paragraph'); ?>
  		  </div>
		  </div><!-- row category-header-->

			<div class="row divider services-title">
        <h2><?php the_field('service_title'); ?></h2>
      </div>
      
      <div class="entry-content">
        <?php while ( have_posts() ) : the_post(); ?>
          <?php the_content(); ?>
        <?php endwhile; // end of the loop. ?>
		  </div>
	
	
	<div class="row">
<div class="home-title">
    <h2>THE HATCHERY</h2>
</div>

<div id="post-content">
    <div class="content-area row bxslider">
    
        <?php $args = array(
                'post_type'           => 'post',
                'ignore_sticky_posts' => true,
                'posts_per_page'      => 12,
                'category_name'       => get_the_title(),
            );

        $postrow = new WP_Query($args);

if ($postrow->have_posts()) : ?>
    
            <?php /* Start the Loop */ ?>
            <?php while ($postrow->have_posts()) :
                $postrow->the_post(); ?>
                <?php get_template_part('content', 'grid'); ?>
            <?php endwhile; ?>
           
        <?php else : ?>
            <?php get_template_part('content', 'none'); ?>
        <?php endif;
wp_reset_postdata(); ?>
    
    </div>
    <!-- .posts -->
    <div class="content">
        <div class="home-cta">
            <a href="/blog/">See All</a>
        </div>
    </div>
</div>

</div>
  <?php get_template_part( 'parts/services-cta' ); ?>   
                                
  </main><!-- #main -->
            
</section><!-- #primary -->

<?php get_footer(); ?>
