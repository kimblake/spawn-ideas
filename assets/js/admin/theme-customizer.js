/**
 * This file adds some LIVE to the Theme Customizer live preview. To leverage
 * this, set your custom settings to 'postMessage' and then add your handling
 * here. Your javascript should grab settings from customizer controls, and
 * then make any necessary changes to the page using jQuery.
 */
( function( $ ) {

	// Update the site title in real time...
	window.wp.customize( 'blogname', function( value ) {
		value.bind( function( newval ) {
			$( '#site-title a' ).html( newval );
		} );
	} );

	//Update the site description in real time...
	window.wp.customize( 'blogdescription', function( value ) {
		value.bind( function( newval ) {
			$( '.site-description' ).html( newval );
		} );
	} );

	//Update the hero heading in real time...
	window.wp.customize( 'spawn_headline_text', function( value ) {
		value.bind( function( newval ) {
			$( '#js-banner-text' ).html( newval );
		} );
	} );

	//Update the hero heading in real time...
	window.wp.customize( 'spawn_category_title_text', function( value ) {
		value.bind( function( newval ) {
			$( '#js-category-title' ).html( newval );
		} );
	} );

	//Update the hero heading in real time...
	window.wp.customize( 'spawn_featured_category_text', function( value ) {
		value.bind( function( newval ) {
			$( '#js-category-text' ).html( newval );
		} );
	} );

	//Update the hero heading in real time...
	window.wp.customize( 'spawn_featured_category_link_text', function( value ) {
		value.bind( function( newval ) {
			$( '#js-category-link' ).html( newval );
		} );
	} );

	//Update the hero heading in real time...
	window.wp.customize( 'spawn_best_places_title', function( value ) {
		value.bind( function( newval ) {
			$( '#js-best-places-text' ).html( newval );
		} );
	} );

	//Update the hero heading in real time...
	window.wp.customize( 'spawn_best_places_text', function( value ) {
		value.bind( function( newval ) {
			$( '#js-best-places-link' ).html( newval );
		} );
	} );

	//Update the hero heading in real time...
	window.wp.customize( 'spawn_awards_text', function( value ) {
		value.bind( function( newval ) {
			$( '#js-awards-text' ).html( newval );
		} );
	} );

} )( jQuery );