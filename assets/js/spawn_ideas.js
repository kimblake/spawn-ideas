/*! Spawn Ideas - v0.1.0 - 2014-11-18
 * Copyright (c) 2014; * Licensed GPLv2+ */
 
/* this handles sliders conditionally */


jQuery(document).ready(function ($) {

    /*** Enables menu toggle for small screens */

    var nav = $('#site-navigation'),
        button, menu;
    if (!nav) {
        return;
    }

    button = nav.find('.menu-toggle');
    if (!button) {
        return;
    }

    // Hide button if menu is missing or empty.
    menu = nav.find('.nav-menu');
    if (!menu || !menu.children().length) {
        button.hide();
        return;
    }

    button.on('click.spawn', function () {
        nav.toggleClass('toggled-on');
    });


    // Better focus for hidden submenu items for accessibility.
    menu.find('a').on('focus.spawn blur.spawn', function () {
        $(this).parents('.menu-item, .page_item').toggleClass('focus');
    });


    var is_webkit = navigator.userAgent.toLowerCase().indexOf('webkit') > -1,
        is_opera = navigator.userAgent.toLowerCase().indexOf('opera') > -1,
        is_ie = navigator.userAgent.toLowerCase().indexOf('msie') > -1;

    if (( is_webkit || is_opera || is_ie ) && document.getElementById && window.addEventListener) {
        window.addEventListener('hashchange', function () {
            var element = document.getElementById(location.hash.substring(1));

            if (element) {
                if (!/^(?:a|select|input|button|textarea)$/i.test(element.tagName))
                    element.tabIndex = -1;

                element.focus();
            }
        }, false);
    }

    (function (window, document, $, undefined) {
        'use strict';

        $('#filter-toggle').on('click', function () {
            $('#filtered').toggleClass('hidden');
        });
    })(window, document, jQuery);

    function spawn_ajax_get(catID) {
        var currPosition = jQuery(window).scrollTop();

        $("a.ajax").removeClass("current");
        $("a.ajax").addClass("current"); //adds class current to the category menu item being displayed so you can style it with css
        $("#loading-animation").show();
        $.ajax({
            type: 'POST',
            url: SpawnAjax.ajaxurl,
            data: {
                action: "load_filter",
                cat: catID,
                postType: SpawnAjax.postType
            },
            success: function (response) {
                $("#portfolio-content").html(response);
                $("#loading-animation").hide();
                $(window).scrollTop(currPosition);
                return false;
            }
        });
    }

    $('#project-sidebar').prependTo('.cfct-build');

    $('.leaderbio').on('click', function () {
        $('.bio-full', this).toggle();
    });


    var $body = $('body');
    if ($body.hasClass('home')) {
        $("#work-slider").responsiveSlides({
            auto: true,
            pager: true,
            speed: 800,
            timeout: 5000
        });
    }

    // Adjust the width & height of some portfolio blocks at load time to account for auto sizing
    var w = $(window).width();
    if ($body.hasClass('single-spawn-projects') && w >= 1130) {
        var mods = $('.c6-3456, .c6-1234, .c4-12, .c4-34').find('img');
        var width, height;
        for (var i = 0; i < mods.length; i++) {
            width = $(mods[i]).innerWidth();
            if (width < 310) width = 300;
            $(mods[i]).css({'min-width': width, 'max-wdith': width});

            $(mods[i]).css({'min-height': 300});
            height = $(mods[i]).attr('height');
            height < 310 ? height = 300 : height = 600;
            $(mods[i]).css({'max-height': height, 'min-height': height});
            $(mods[i]).outerHeight(false);
        }
    }


    // fluidvids.init({
    //   selector: ['iframe'],
    //   players: ['www.youtube.com', 'player.vimeo.com']
    // });

    function AdjustVids() {
        var iframes = document.getElementsByTagName('iframe');
        for (var i = 0; i < iframes.length; i++) {
            var iframe = iframes[i];
            var players = /www.youtube.com|player.vimeo.com/;
            if (iframe.src.search(players) !== -1) {
                if (!iframe.getAttribute('orig-width')) {
                    console.log(iframe.width, iframe.height);
                    iframe.setAttribute('orig-width', iframe.width);
                    iframe.setAttribute('orig-height', iframe.height);
                    iframe.removeAttribute('width');
                    iframe.removeAttribute('height');
                }
                var aspect = parseInt(iframe.getAttribute('orig-height')) / parseInt(iframe.getAttribute('orig-width'));
                var width;
                if ($(window).width() <= 540) {
                    width = $(iframe.parentNode).innerWidth();
                } else {
                    width = $(iframe.offsetParent).innerWidth();
                }
                console.log('width: ' + width + ', height: ' + width * aspect);
                iframe.style.width = width + 'px';
                iframe.style.height = '' + ( aspect * width ) + 'px';
            }
        }
    }

    var w = $(window).width();
    if ($body.hasClass('single-spawn-projects') && w < 992) {
        $(window).resize(AdjustVids);
        AdjustVids();
        // original
        //        $('.cfct-module').fitVids();
    }

    // Open all external links in a new
    $('a').each(function () {
        var a = new RegExp('/' + window.location.host + '/');
        if (!a.test(this.href)) {
            $(this).click(function (event) {
                event.preventDefault();
                event.stopPropagation();
                window.open(this.href, '_blank');
            });
        }
    });

    if ($body.hasClass('post-type-archive-spawn-projects')) {
        var urlParams;
        ( window.onpopstate = function () {
            var match, pl, search, decode, query;
            pl = /\+/g;
            search = /([^&=]+)=?([^&]*)/g;
            decode = function (s) {
                return decodeURIComponent(s.replace(pl, " "));
            };
            query = window.location.search.substring(1);

            urlParams = {};
            while (match = search.exec(query))
                urlParams[decode(match[1])] = decode(match[2]);
        } )();

        if (urlParams['work']) spawn_ajax_get(urlParams['work']);
    }

    // Hide and show header menu on home page
    if ($body.hasClass('home')) {
        $(window).scroll(function () {
            if ($(this).scrollTop() < 485) {
                //              $( '.header-menu' ).hide();
                //              $( '.hero-menu' ).show();
                $body.removeClass('hero-scroll-hidden');
            } else {
                //              $( '.header-menu' ).show();
                //              $( '.hero-menu' ).hide();
                $body.addClass('hero-scroll-hidden');
            }
        });
    }



    var loc = top.location.pathname; //gets url path
    var bsslider = $('.bxslider'); // gets element

    if (bsslider.length > 0) {
        //markup for slider buttons
        var nextIcon = '<i class="fa fa-chevron-right fa-2"></i>';
        var prevIcon = '<i class="fa fa-chevron-left fa-2"></i>';
        var nextNav = '<div id="slider-next" class="right-nav"><span></span></div>';
        var prevNav = '<div id="slider-prev" class="left-nav"><span></span></div>';

        if (bsslider.length != null) {
            //geting links ready for population
            $('.content-area.row').each(function (i) {
                var i = i + 1;
                $(this).parents('.row').addClass('bxslider-row');
                $(this).wrap('<div class="row"></div>').before(prevNav, nextNav);
                $('.bxslider-row' + ' #slider-prev').attr('id', 'slider-prev-' + i);
                $('.bxslider-row' + ' #slider-next').attr('id', 'slider-next-' + i);
            });

            $('.bxslider').bxSlider({
                prevSelector: '#slider-prev-1 span', // custom id or class for selectors
                nextSelector: '#slider-next-1 span', // custom id or class for selectors
                prevText: prevIcon, //icon for prev anchor button
                nextText: nextIcon, //icon for next anchor button
                pager: false,
                autoreload: true,
                slideWidth: 250, // setting container width
                minSlides: 2, // base number of required slides
                maxSlides: 4, // limited to what can be viewed
                startSlider: 2,
                moveSlides: 4, // action count of slides presented on slider rotation
                slideMargin: 10 // gap between slides
            });
        } 
        else {
            console.log(bsslider + ' does not exist');
        }

        if ($('.bxslider2').length != null ) {
            $('.bxslider2').bxSlider({
                prevSelector: '#slider-prev-2 span', // custom id or class for selectors
                nextSelector: '#slider-next-2 span', // custom id or class for selectors
                prevText: prevIcon, //icon for prev anchor button
                nextText: nextIcon, //icon for next anchor button
                pager: false,
                autoreload: true,
                slideWidth: 250, // setting container width
                minSlides: 2, // base number of required slides
                maxSlides: 4, // limited to what can be viewed
                startSlider: 2,
                moveSlides: 4, // action count of slides presented on slider rotation
                slideMargin: 10 // gap between slides
            });
        } else {
            console.log(bsslider + 2 + ' does not exist');
        }
    }

});