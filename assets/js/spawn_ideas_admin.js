/*! Spawn Ideas - v0.1.0 - 2014-11-18
 * http://10up.com
 * Copyright (c) 2014; * Licensed GPLv2+ */
(function ( window, document, $, undefined ) {
	'use strict';

	/**
	 * Upload Assets
	 */
	function uploadAssetHandler( e ) {
		var button = $(this);
		var media;

		if ( !wp ) {
			return;
		}
		e.preventDefault();

		var mediaType = 'Asset';

		if ( 'image' === $( this ).attr( 'ref' ) ) {
			mediaType = 'Image'
		}

		if ( media  ) {
			media.open();
			return;
		}

		media = wp.media.frames.file_frame = wp.media( {
			title   : 'Upload ' + mediaType,
			button  : {
				text: 'Add ' + mediaType
			},
			multiple: false
		} );

		media.on( 'select', function () {
			var selection = media.state().get( 'selection' );
			var attachment = selection.first().toJSON();
			console.log(attachment);
			var asset = '';
			var asset_field = button.prev('#tax_image');
			asset_field.val( attachment.url );
			var hidden_field = $('#tax_image_id');
			hidden_field.val( attachment.id );

			asset = '<div class="img_status"><img style="max-width: 350px; width: 100%; height: auto;" src="' + attachment.url + '" /><p><a href="#" class="remove_file_button"> Remove Image </a></p></div>';

			asset_field.siblings( '#asset' ).slideDown().html( asset );
		} );

		media.open();
	}

	/**
	 * Remove Assets
	 * @TODO: update remove function to clear ID as well as url.
	 */
	function removeAssetHandler( e ) {
		e.preventDefault();
		$( document.getElementById( 'tax_image' ) ).empty();
		asset_field.val( '' );
	}

	/**
	 * Clear custom fields in custom taxonomies
	 */
	function clearTaxonomyFormHandler( e ) {
		e.preventDefault();
		var form = $( this ).parents( 'form' );

		if ( validateForm( form ) ) {
			$( document.getElementById( 'tax_image' ) ).empty();
		}
	}

	/**
	 * Add Brand Link Row
	 */
	function addLinkHandler( e ) {
		e.preventDefault();
		var table = $(this ).parent().parent().find('table');
		table.append( '<tr class="sort_link">\
			<td valign="top"><input type="text" class="widefat" name="links[label][]" id="link" value="" /></td>\
			<td valign="top"><input type="text" class="widefat" name="links[url][]" id="url" value="" /> </td>\
			<td valign="top"><a href="" class="button delete_link">Delete</a></td> </tr>' );
		table.find( 'tr' ).first().show();
	}

	/**
	 * Delete Brand Link Row
	 */
	function removeLinkHandler( e ) {
		e.preventDefault();
		$( this ).parent().parent().remove();
	}

	$( document.getElementById( 'upload_button' ) ).on( 'click', uploadAssetHandler );
	$( '.upload_taxonomy_image' ).on( 'click', uploadAssetHandler );
	$( document.getElementById( 'wpbody-content' ) ).on( 'click', '.remove_file_button', removeAssetHandler );
	$( document.getElementById( 'add_link' ) ).on( 'click', addLinkHandler );
	$( document.getElementById( 'addtag' ) ).find( document.getElementById( 'submit' ) ).on( 'click', clearTaxonomyFormHandler );

})( window, document, jQuery );
( function( $ ) {

	// Update the site title in real time...
	window.wp.customize( 'blogname', function( value ) {
		value.bind( function( newval ) {
			$( '#site-title a' ).html( newval );
		} );
	} );

	//Update the site description in real time...
	window.wp.customize( 'blogdescription', function( value ) {
		value.bind( function( newval ) {
			$( '.site-description' ).html( newval );
		} );
	} );

	//Update the hero heading in real time...
	window.wp.customize( 'spawn_headline_text', function( value ) {
		value.bind( function( newval ) {
			$( '#js-banner-text' ).html( newval );
		} );
	} );

	//Update the hero heading in real time...
	window.wp.customize( 'spawn_category_title_text', function( value ) {
		value.bind( function( newval ) {
			$( '#js-category-title' ).html( newval );
		} );
	} );

	//Update the hero heading in real time...
	window.wp.customize( 'spawn_featured_category_text', function( value ) {
		value.bind( function( newval ) {
			$( '#js-category-text' ).html( newval );
		} );
	} );

	//Update the hero heading in real time...
	window.wp.customize( 'spawn_featured_category_link_text', function( value ) {
		value.bind( function( newval ) {
			$( '#js-category-link' ).html( newval );
		} );
	} );

	//Update the hero heading in real time...
	window.wp.customize( 'spawn_best_places_title', function( value ) {
		value.bind( function( newval ) {
			$( '#js-best-places-text' ).html( newval );
		} );
	} );

	//Update the hero heading in real time...
	window.wp.customize( 'spawn_best_places_text', function( value ) {
		value.bind( function( newval ) {
			$( '#js-best-places-link' ).html( newval );
		} );
	} );

	//Update the hero heading in real time...
	window.wp.customize( 'spawn_awards_text', function( value ) {
		value.bind( function( newval ) {
			$( '#js-awards-text' ).html( newval );
		} );
	} );

} )( jQuery );