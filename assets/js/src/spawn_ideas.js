/**
 * Spawn Ideas
 * http://10up.com
 *
 * Copyright (c) 2014 Amy Hendrix
 * Licensed under the GPLv2+ license.
 */

 ( function( window, document, $, undefined ) {
	'use strict';

	$('#filter-toggle').on('click', function(){
		$('#filtered').toggleClass('hidden');
	});
} )( window, document, jQuery );

function spawn_ajax_get(catID) {
	var currPosition = jQuery(window).scrollTop();

	jQuery("a.ajax").removeClass("current");
	jQuery("a.ajax").addClass("current"); //adds class current to the category menu item being displayed so you can style it with css
	jQuery("#loading-animation").show();
	jQuery.ajax({
		type: 'POST',
		url: SpawnAjax.ajaxurl,
		data: {
			action: "load_filter",
			cat: catID,
			postType: SpawnAjax.postType
		},
		success: function(response) {
			jQuery("#portfolio-content").html(response);
			jQuery("#loading-animation").hide();
			jQuery(window).scrollTop(currPosition);
			return false;
		}
	});
}

jQuery(document).ready(function($){
	$('#project-sidebar').prependTo('.cfct-build');
});

jQuery(document).ready(function($){
	$('.leaderbio').on('click', function(){
		$('.bio-full', this).toggle();
	});
});


jQuery(document).ready(function($) {
	if ( jQuery('body').hasClass('home') ) {
		$("#work-slider").responsiveSlides({
			auto: true,
			pager: true,
			speed: 800,
            timeout: 5000
		});
	}
});

// Adjust the width & height of some portfolio blocks at load time to account for auto sizing
jQuery(document).ready(function($) {
    var w = $(window).width();
    if ( $('body').hasClass('single-spawn-projects') && w >= 1130 ) {
        var mods = $( '.c6-3456, .c6-1234, .c4-12, .c4-34' ).find( 'img' );
        var width, height;
        for ( var i = 0; i < mods.length; i++ ) {
            width = $( mods[i] ).innerWidth();
            if ( width < 310 ) width = 300;
            $( mods[i] ).css( { 'min-width': width, 'max-wdith' : width });

            $( mods[i] ).css( { 'min-height': 300 });
            height = $( mods[i] ).attr('height');
            height < 310 ? height = 300 : height = 600;
            $( mods[i] ).css( { 'max-height': height, 'min-height' : height } );
            $( mods[i] ).outerHeight(false);
        }
    }
});

jQuery(document).ready(function($){
    var w = $(window).width();
    if( $('body').hasClass('single-spawn-projects') && w < 992 ) {
        $('.cfct-module').fitVids();
    }
});

// Open all external links in a new
jQuery('a').each(function() {
	var a = new RegExp('/' + window.location.host + '/');
	if(!a.test(this.href)) {
		jQuery(this).click(function(event) {
			event.preventDefault();
			event.stopPropagation();
			window.open(this.href, '_blank');
		});
	}
});

jQuery(document).ready(function() {
	if ( jQuery('body').hasClass('post-type-archive-spawn-projects') ) {
		var urlParams;
		(window.onpopstate = function () {
			var match,
				pl     = /\+/g,  // Regex for replacing addition symbol with a space
				search = /([^&=]+)=?([^&]*)/g,
				decode = function (s) { return decodeURIComponent(s.replace(pl, " ")); },
				query  = window.location.search.substring(1);

			urlParams = {};
			while (match = search.exec(query))
				urlParams[decode(match[1])] = decode(match[2]);
		})();

		if ( urlParams['work'] ) {
			spawn_ajax_get( urlParams['work'] );
		}
	}
});

// Hide and show header menu on home page
jQuery( document ).ready( function() {
	if( jQuery( 'body' ).hasClass( 'home' ) ) {
		jQuery( window ).scroll( function(){
			if ( jQuery( this ).scrollTop() < 485 ) {
				jQuery( '.header-menu' ).hide();
				jQuery( '.hero-menu' ).show();
			} else {
				jQuery( '.header-menu' ).show();
				jQuery( '.hero-menu' ).hide();
			}
		});
	}
});




