<?php
/**
 * Template Name: Services
 */

get_header(); ?>

	<section id="primary" class="content-area">
		<main id="main" class="site-main" role="main">
					
    <?php get_template_part( 'parts/services-menu-parent' ); ?> 			
            			
		
		<?php get_template_part( 'parts/services-cta' ); ?>

		</main><!-- #main -->
	</section><!-- #primary -->

<?php get_footer(); ?>