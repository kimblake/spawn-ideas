<?php
/**
 * The blog template file
 *
 * @package Spawn Ideas
 * @since 0.1.0
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<div class="page-header">
			<h1>The Hatchery</h1>
		</div>

		<main id="main" class="site-main" role="main">

		<?php if ( have_posts() ) : ?>

		<?php
/**	<div class="project-filter">
				<button id="filter-toggle">Filter</button>
				<div class="hidden filters" id="filtered">
					<div class="filter-list">
						<h5><?php esc_html_e( 'categories', 'spawn' ); ?></h5>
						<?php
							$industries = get_terms( 'category', array( 'post_type' => 'post' ) );
							if ( ! empty( $industries ) && ! is_wp_error( $industries ) ){
								echo "<ul>";
								foreach ( $industries as $industry ) { ?>
									<li id="industry-<?php esc_html_e( $industry->term_id, 'spawn' ); ?>"><a class="<?php esc_html_e( $industry->slug, 'spawn'); ?> ajax" onclick="spawn_ajax_get('<?php esc_html_e( $industry->term_id, 'spawn' ); ?>');" href="#"><?php esc_html_e( $industry->name, 'spawn' ); ?></a></li><?php
								}
								echo "</ul>";
							}
						 ?>
					</div>
					<div class="filter-list">
						<h5><?php esc_html_e( 'services', 'spawn' ); ?></h5>
						<?php
							$services = get_terms( 'services', array( 'post_type' => 'post' ) );
							if ( !empty( $services ) && !is_wp_error( $services ) ){
								echo "<ul>";
								foreach ( $services as $service ) {?>
									<li id="service-<?php esc_html_e( $service->term_id, 'spawn' ); ?>"><a class="<?php esc_html_e( $service->slug, 'spawn' ); ?> ajax" onclick="spawn_ajax_get('<?php esc_html_e( $service->term_id, 'spawn' ); ?>');" href="#"><?php esc_html_e( $service->name, 'spawn' ); ?></a></li><?php
								}
								echo "</ul>";
							}
						?>
					</div>
					<div class="filter-list">
						<h5><?php esc_html_e( 'keywords', 'spawn' ); ?></h5>
						<?php
							$keywords = get_terms( 'keywords', array( 'post_type' => 'post' ) );
							if ( !empty( $keywords ) && !is_wp_error( $keywords ) ){
								echo "<ul>";
								foreach ( $keywords as $keyword ) { ?>
									<li id="keyword-<?php esc_html_e( $keyword->term_id, 'spawn' ); ?>"><a class="<?php esc_html_e( $keyword->slug, 'spawn' ); ?> ajax" onclick="spawn_ajax_get('<?php esc_html_e( $keyword->term_id, 'spawn' ); ?>');" href="#"><?php esc_html_e( $keyword->name, 'spawn' ); ?></a></li><?php
								}
								echo "</ul>";
							}
						?>
					</div>
				</div>

			</div>*/ ?>

			<?php /* Start the Loop */ ?>
			<div id="loading-animation" style="display: none;"><img src="<?php echo esc_url( get_template_directory_uri() ) . '/images/loader.gif'; ?>"/></div>
			<div id="portfolio-content">

			<?php /* Start the Loop */ ?>
			<?php while ( have_posts() ) : the_post(); ?>

				<?php
					/* Include the Post-Format-specific template for the content.
					 * If you want to override this in a child theme, then include a file
					 * called content-___.php (where ___ is the Post Format name) and that will be used instead.
					 */
					get_template_part( 'content', 'list' );
				?>

			<?php endwhile; ?>

			<?php spawn_paging_nav(); ?>
			</div>
		<?php else : ?>

			<?php get_template_part( 'content', 'none' ); ?>

		<?php endif; ?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php get_footer(); ?>
