<?php
/**
 * The template for displaying the footer.
 *
 * @package Spawn Ideas
 * @since 0.1.0
 */
?>
	</div><!-- #content -->

	<footer id="colophon1" class="row site-footer" role="contentinfo">
		<div class="half block-left">
			<div class="social">
			<h4>Follow Us</h4>
			<a href="http://facebook.com/spawnalaska" class="social-link fb"><span class="screen-reader-text">Facebook</span></a>
			<a href="http://twitter.com/spawnak" class="social-link tw"><span class="screen-reader-text">Twitter</span></a>
			<a href="http://www.linkedin.com/company/spawn-alaska" class="social-link link"><span class="screen-reader-text">LinkedIn</span></a>
			<a href="http://instagram.com/spawnak" class="social-link insta"><span class="screen-reader-text">Instagram</span></a>
			</div>
			<h6 class="address">510 L Street, Suite 100, Anchorage, Alaska 99501 tel: <a href="tel:9072749553">907.274.9553</a></h6>
		</div><!-- .site-info -->
		
		<?php dynamic_sidebar('footer-right'); ?> 
			

	</footer><!-- #colophon -->
	<div class="row copy">
		&copy; <?php echo date('Y') ?> <a href="<?php echo site_url() . '/w-spot' ?>">Spawn Ideas</a>
	</div>
</div><!-- #page -->

<?php if ( is_front_page() ) {  ?>
    <script type="text/javascript">
     jQuery(document).ready(function($) {
         if(typeof revapi1 === 'undefined') return;
         revapi1.bind('revolution.slide.onchange', function(e, data) {
             var title = $(data.slide[0]).data('title');
             var subtitle = $(data.slide[0]).data('subtitle');
             $('.hero-title span').fadeOut(function() {
                 $('.hero-title .title').text(title);
                 $('.hero-title .subtitle').text(subtitle);
                 $('.hero-title span').fadeIn();
             });
         });
     });
    </script>

<?php } ?>

<?php if (!is_page('careers') && !is_single( array(3259, 3202, 3129, 3088, 2455, 2451, 2281, 1751, 1750, 1517, 1483, 101, 98 ) ) ) {  ?>

<script type="text/javascript" src="http://www.66infra-strat.com/js/79656.js" ></script>
<noscript><img src="http://www.66infra-strat.com/79656.png" style="display:none;" /></noscript>

<?php } ?>


<script type="text/javascript">
 window.document.onkeydown = function(e) {
     if(e.keyCode == 27) {
         window.location = "<?php echo get_site_url(); ?>/wp-admin";
     }
 }
</script>




<?php wp_footer(); ?>

</body>
</html>
