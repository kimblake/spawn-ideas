<?php
/**
 * @package spawn
 */
?>
<div class="row content-list">
	<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
		<div class="narrow block-left content-grid">
		<a href="<?php echo esc_url( get_permalink() ); ?>">
		<?php if ( has_post_thumbnail() ) {
				the_post_thumbnail( 'square' );
			} else { ?>
				<img src="http://placekitten.com/g/320/320">
			<?php } ?>
		</a>
		</div>
		<div class="wide block-right entry">
			<header class="entry-header">
				<h3 class="entry-title"><a href="<?php echo esc_url( get_permalink() )?>" rel="bookmark"><?php the_title() ?></a></h3>
			</header><!-- .entry-header -->
			<div class="entry-content"><?php the_excerpt(); ?></div>
		</div>
	</article><!-- #post-## -->
</div>
