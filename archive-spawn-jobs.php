<?php
/**
 * The template for displaying archive pages.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package spawn
 */

get_header(); ?>

	<section id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

		<?php if ( have_posts() ) : ?>

			<header class="page-header">
				<h1 class="page-title">
					Careers
				</h1>
			</header><!-- .page-header -->

			<div class="row jobs">
				<div class="half block-left jobs-text">
				<p>It’s the people here that make Spawn fertile grounds for success. A diverse bunch of ad pros from every corner of the country. All with their own unique talents and stories to tell. When you get that much diversity in one place, creativity thrives. It also makes for a pretty interesting happy hour.</p>
				</div>
				<div class="half block-right">
					<?php if ( has_post_thumbnail() ) {
						the_post_thumbnail( 'half-width' );
					} else { ?>
						<img src="http://lorempixel.com/g/600/320/">
					<?php } ?>
				</div>
			</div>

		<?php
		/**
		 * INSTAGRAM WIDGET
		 */
		$mp_instagram_parameter = '@spawnak';
		if ( ! empty( $mp_instagram_parameter ) ) {
			$mp_instagram = new MP_Instagram_Widget;
			$mp_instagram_output = $mp_instagram->get_instagram_widget( $mp_instagram_parameter );


			if ( ! empty( $mp_instagram_output ) )
				echo $mp_instagram_output;
		}
		?>

		<div class="row divider">
			<h3>Open Jobs</h3>
		</div>
		<section class="row jobs-list">
			<?php /* Start the Loop */ ?>
			<?php while ( have_posts() ) : the_post(); ?>

				<?php
					get_template_part( 'content', 'job' );
				?>

			<?php endwhile; ?>

			<?php spawn_paging_nav(); ?>
		</section>
		<?php else : ?>

			<?php get_template_part( 'content', 'none' ); ?>

		<?php endif; ?>

		</main><!-- #main -->
	</section><!-- #primary -->

<?php get_footer(); ?>
